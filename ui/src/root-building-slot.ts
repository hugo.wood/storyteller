import {LitElement, html} from 'lit';
import {customElement} from 'lit/decorators.js';

@customElement('root-building-slot')
export class RootBuildingSlot extends LitElement {
    override render() {
        return html`
            <style>
                :host {
                    display: block;
                    width: 100%;
                    height: 100%;
                }
                svg * {
                    vector-effect: non-scaling-stroke;
                }
            </style>
            <svg style="width: 100%; height: 100%">
                <defs>
                    <clipPath id="c">
                        <rect x="0%" y="0%" width="100%" height="100%" rx="15%" fill="#000"/>
                    </clipPath>
                </defs>
                <rect clip-path="url(#c)" x="0%" y="0%" width="100%" height="100%" rx="15%" fill="none" stroke="#000" stroke-width="2px"/>
            </svg>
            `;
    }
}
