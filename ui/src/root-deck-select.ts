import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';

@customElement('root-deck-select')
export class RootDeckSelect extends LitElement {
    deckOptions = { base: "Base", enp: "E&P" }

    @property()
    value: string = "Base";

    override render() {
        return html`
            <style>
            :host {
                display: inline-block;
            }
            div {
                display: flex;
                flex-direction: row;
            }
            </style>
            <div>
            <select name="deckName">
                ${map(Object.entries(this.deckOptions), ([value, label]) => html`
                    <option value=${value} ?selected="${value == this.value}">${label}</option>
                    `)}
            </select>
            <div>
        `;
    }
}
