import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {when} from 'lit/directives/when.js';
import {Quantity} from './types';
import './root-icon';
import './root-building';
import './root-building-slot';
import './root-ruins';
import './root-warriors';
import './root-token';

@customElement('root-clearing')
export class RootClearing extends LitElement {
    @property()
    number?: number;

    @property()
    suit: string = "";

    @property({type: Number, attribute: "building-slot-count"})
    buildingSlotCount: number = 1;

    @property()
    pieces: Quantity[] = [];

    warriorPositions = [
        {x: 20, y: 20},
        {x: 80, y: 20},
        {x: 20, y: 80},
        {x: 80, y: 80},
        {x: 50, y: 20},
    ];

    tokenPositions = [
        {x: 10, y: 50},
        {x: 30, y: 70},
        {x: 90, y: 50},
        {x: 70, y: 70},
    ];

    renderRuinsBuildings() {
        const pieces =
            this.pieces
            .filter(({piece}) => piece.building !== undefined || piece.marker === "ruins")
            .flatMap(({piece, count}) => Array(count).fill(piece.building || piece.marker));

        return map(
            Array(this.buildingSlotCount).keys(),
            i => {
                if (i < pieces.length) {
                    const b = pieces[i];
                    if (b === "ruins") {
                        return html`<root-ruins></root-ruins>`;
                    } else {
                        return html`<root-building .building="${b}"></root-building>`;
                    }
                } else {
                    return html`<root-building-slot></root-building-slot>`;
                }
            }
        );
    }

    renderWarriors() {
        const warriors =
            this.pieces
            .filter(({piece, count}) => (piece.warrior !== undefined || piece.pawn !== undefined) && count > 0)
            .map(({piece, count}) => ({faction: piece.warrior || piece.pawn, count}));
        const warlord =
            Boolean(this.pieces.find(({piece, count}) => piece.hundredsPiece == "warlord" && count > 0));
        return map(
            warriors, ({faction, count}, i) => {
                let p = this.warriorPositions[i];
                return html`
                <root-warriors faction="${faction}" count="${count}" ?warlord="${faction == "hundreds" && warlord}" style="position: absolute; top: ${p.y}%; left: ${p.x}%;"></root-warriors>
                `;
            }
        )
    }

    renderTokens() {
        const tokens =
            this.pieces
            .filter(({piece, count}) => piece.token !== undefined && count > 0)
            .map(({piece, count}) => ({token: piece.token, count}))
        return map(
            tokens, ({token, count}, i) => {
                let p = this.tokenPositions[i];
                return html`
                <root-token .token="${token}" count="${count}" style="position: absolute; top: ${p.y}%; left: ${p.x}%;" />
                `;
            }
        )
    }

    override render() {
        return html`
            <style>
                :host {
                    display: block;
                }
                root-building-slot, root-building, root-ruins {
                    height: 35%;
                    width: 35%;
                }
                root-warriors {
                    height: 25%;
                    width: 25%;
                    translate: -50% -50%;
                }
                root-token {
                    height: 20%;
                    width: 20%;
                    translate: -50% -50%;
                }
                svg * {
                    vector-effect: non-scaling-stroke;
                }
                text {
                    font: bold 5pt Arial;
                    fill: #fff;
                    text-anchor: middle;
                }
            </style>
            <div style="position: relative; height: 100%; width: 100%;">
                <svg viewBox="0 0 100 100" style="aspect-ratio: 1 / 1">
                    <circle cx="50" cy="50" r="45" fill="#fff" stroke="#000" stroke-width="2px"/>
                </svg>
                ${when(
                    this.number !== undefined,
                    () => html`
                        <svg viewBox="0 0 15 10" style="position: absolute; top: 0; left: 0; height: 20%; translate: 0 -50%;">
                            <polygon points="0,5 2.5,0 8.5,0 11,5 8.5,10 2.5,10" style="fill: black;"/>
                            <text x="5.5" y="7.2">${this.number}</text>
                        </svg>
                    `
                )}
                ${when(
                    this.pieces.find(({piece, count}) => piece.landmark === "lostCity" && count > 0),
                    () => html`<root-icon icon="lostCity" style="position: absolute; top: 0; left: 50%; translate: -50% -50%; width: 48px; height: 48px; color: var(--lostCity-color);"></root-icon>`,
                    () => html`<root-icon icon="${this.suit}-wborder" style="position: absolute; top: 0; left: 50%; translate: -50% -50%; width: 32px; height: 32px; color: var(--${this.suit}-color); z-index: 4;"></root-icon>`
                )}
                <div style="position: absolute; top: 20%; left: 20%; height: 60%; width: 60%; aspect-ratio: 1 / 1; display: flex; flex-wrap: wrap; align-content: center; justify-content: center; gap: 5% 5%; z-index: 2;">
                    ${this.renderRuinsBuildings()}
                </div>
                ${this.renderWarriors()}
                ${this.renderTokens()}
                ${when(this.pieces.find(({piece, count}) => piece.landmark === "ferry" && count > 0), () => html`<root-icon icon="ferry" style="position: absolute; top: 100%; left: 50%; translate: -50% -50%; width: 30%; height: 30%; color: var(--ferry-color);"></root-icon>`)}
                ${when(this.pieces.find(({piece, count}) => piece.landmark === "tower" && count > 0), () => html`<root-icon icon="tower" style="position: absolute; top: 80%; left: 50%; translate: -50% -50%; width: 30%; height: 30%; color: var(--tower-color);"></root-icon>`)}
            </div>
            `;
    }
}
