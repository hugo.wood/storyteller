import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {Quantity} from './types';
import './root-icon';
import './root-building';
import './root-building-slot';
import './root-warriors';
import './root-token';

@customElement('root-forest')
export class RootForest extends LitElement {
    @property()
    pieces: Quantity[] = [];

    warriorPositions = [
        {x: 30, y: 40},
        {x: 60, y: 40},
        {x: 40, y: 60},
        {x: 60, y: 60},
    ];

    tokenPositions = [
        {x: 50, y: 30},
        {x: 30, y: 50},
        {x: 50, y: 50},
        {x: 30, y: 30},
    ];

    renderWarriors() {
        const warriors =
            this.pieces
            .filter(({piece, count}) => (piece.warrior !== undefined || piece.pawn !== undefined) && count > 0)
            .map(({piece, count}) => ({faction: piece.warrior || piece.pawn, count}));
        return map(
            warriors, ({faction, count}, i) => {
                let p = this.warriorPositions[i];
                return html`
                <root-warriors faction="${faction}" count="${count}" style="position: absolute; top: ${p.y}%; left: ${p.x}%;"></root-warriors>
                `;
            }
        )
    }

    renderTokens() {
        const tokens =
            this.pieces
            .filter(({piece, count}) => piece.token !== undefined && count > 0)
            .map(({piece, count}) => ({token: piece.token, count}))
        return map(
            tokens, ({token, count}, i) => {
                let p = this.tokenPositions[i];
                return html`
                <root-token .token="${token}" count="${count}" style="position: absolute; top: ${p.y}%; left: ${p.x}%;" />
                `;
            }
        )
    }

    override render() {
        return html`
            <style>
                :host {
                    display: block;
                }
                root-warriors {
                    height: 25%;
                    width: 25%;
                    translate: -50% -50%;
                }
                root-token {
                    height: 20%;
                    width: 20%;
                    translate: -50% -50%;
                }
                svg * {
                    vector-effect: non-scaling-stroke;
                }
            </style>
            <div style="position: relative; height: 100%; width: 100%;">
                <svg viewBox="0 0 100 100" style="aspect-ratio: 1 / 1">
                    <circle cx="50" cy="50" r="45" fill="none" stroke="none"/>
                </svg>
                ${this.renderWarriors()}
                ${this.renderTokens()}
            </div>
            `;
    }
}
