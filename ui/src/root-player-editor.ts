import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {Faction} from './types';
import './root-icon';
import {RootFactionSelect} from './root-faction-select';
import './root-faction-select';

@customElement('root-player-editor')
export class RootPlayerEditor extends LitElement {
    @property()
    faction?: Faction;

    @property({type: String})
    name?: string;

    @property({type: Object})
    score?: any;

    override render() {
        return html`
            <style>
                :host {
                    display: block;
                }
                root-icon {
                    width: 2em;
                    height: 2em;
                }

                input {
                    flex-shrink: 1;
                    flex-grow: 1;
                    width: 120px;
                }
                root-faction-select, span {
                    flex-shrink: 0;
                    flex-grow: 2;
                }
            </style>
            <div style="background-color: ${this.backgroundColor()}; color: white; display: flex; flex-wrap: nowrap; column-gap: 10px;">
                <root-faction-select faction="${this.faction}" @change="${this._onFactionChange}"></root-faction-select>
                <input value="${this.name}" @input="${this._onNameChange}" />
                <span>${this.renderPlayerScore()}</span>
                <slot></slot>
            </div>
            `;
    }

    private backgroundColor() {
        if (this.faction) {
            return `var(--${this.faction}-color)`;
        } else {
            return "lightgray";
        }
    }

    renderPlayerScore() {
        if (this.score !== undefined) {
            if (this.score.points !== undefined) {
                return html`${this.score.points}pts`;
            } else if (this.score.dominance !== undefined) {
                return html`<root-icon icon="${this.score.dominance}" style="color: white"></root-icon>`;
            } else if (this.score.coalition !== undefined) {
                return html`<root-icon icon="${this.score.coalition}" style="color: white"></root-icon>`;
            } else {
                return html``;
            }
        } else {
            return html`0pts`;
        }
    }

    private _onFactionChange(event: Event) {
        this.faction = (event.target as RootFactionSelect).faction;
        this.dispatchChange();
    }

    private _onNameChange(event: Event) {
        this.name = (event.target as HTMLInputElement).value;
        this.dispatchChange();
    }

    private dispatchChange() {
        this.dispatchEvent(new Event('change'));
    }
}
