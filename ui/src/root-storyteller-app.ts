import {LitElement, html} from 'lit';
import {customElement, property, query} from 'lit/decorators.js';
import {RootStoryteller} from './root-storyteller';
import './root-storyteller';
import './root-file-download';
import {RootFileUpload} from './root-file-upload';
import './root-file-upload';
import init, {parseAction, states, turns_text, dump_game, load_game} from 'storyteller-domain';
import {GameRecord, State, Turn, TurnText} from './types';
import {produce} from 'immer';

@customElement('root-storyteller-app')
export class RootStorytellerApp extends LitElement {
    @property()
    gameRecord: GameRecord = {
        mapName: "autumn",
        deckName: "enp",
        landmarks: [],
        turns: [],
    }

    @property()
    turns: TurnText[] = [];

    @property()
    states: Turn[] = [];

    @query("#downloadLink")
    downloadLink!: HTMLAnchorElement;

    override async connectedCallback() {
        super.connectedCallback();
        await init();
        window.addEventListener("hashchange", this._onHashChange);
    }

    override disconnectedCallback() {
        window.removeEventListener("hashchange", this._onHashChange);
        super.disconnectedCallback();
    }

    override render() {
        return html`
        <style>
            :host {
                display: block;
            }
            root-storyteller {
                width: 100%;
                height: 100%;
            }
        </style>
        <root-storyteller .gameRecord="${this.gameRecord}" .turns="${this.turns}" .state="${this.currentState()}" @change="${this._onChange}">
            <root-file-download slot="tools" @download="${this._onDownload}"></root-file-download>
            <root-file-upload slot="tools" @upload="${this._onUpload}"></root-file-upload>
        </root-storyteller>
        <a id="downloadLink" download="root-game.txt" style="display: none"></a>
        `;
    }

    private currentState(): State {
        const url = new URL(window.location.href);
        let turnNumber: number, actionNumber: number;
        if (url.hash) {
            [turnNumber, actionNumber] = url.hash.replace("#", "").split("/").map(Number);
        } else {
            [turnNumber, actionNumber] = this.states.flatMap((turn, i) => turn.states.map((_, j) => [i, j])).slice(-1)[0] || [undefined, undefined];
        }

        if (turnNumber !== undefined && actionNumber !== undefined) {
            return this.states[turnNumber].states[actionNumber];
        } else {
            return {piecesByLocation: [], scores: {}}
        }
    }

    private _onChange(event: Event) {
        const storyteller = event.target as RootStoryteller;

        this.turns = storyteller.turns;
        this.gameRecord = produce(storyteller.gameRecord, (draft) => {
            draft.turns = this.turns.map((turn: any) => {
                return {
                    faction: turn.faction,
                    actions: turn.actions.flatMap((action: any) => {
                        try {
                            return [parseAction(turn.faction, action)];
                        } catch (e) {
                            console.log(e);
                            return [];
                        }
                    })
                };
            })
        });
        this.states = states(this.gameRecord);
    }

    private _onDownload() {
        const content = dump_game(this.gameRecord);
        const blob = new Blob([content], {type: "text/plain"});
        const url = URL.createObjectURL(blob)
        this.downloadLink.href = url;
        this.downloadLink.click();
        URL.revokeObjectURL(url);
    }

    private _onUpload(event: Event) {
        const content = (event.target as RootFileUpload).content;
        if (content) {
            this.gameRecord = load_game(content);
            this.turns = turns_text(this.gameRecord);
            this.states = states(this.gameRecord);
        }
    }

    private _onHashChange = () => {
        this.requestUpdate();
    }
}
