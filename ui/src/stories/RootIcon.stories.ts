import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import { map } from 'lit/directives/map.js';
import '../root-icon';
import '../root.css';

export default {
  title: 'Root/Atoms/Icon',
} as Meta;

export const All = () => html`
    <style>
    root-icon {
        width: 32px;
        height: 32px;
        color: gray;
    }
    </style>
    <h2>Suits</h2>
    <div style="display: flex; flex-flow: row wrap;">
        <root-icon icon="fox"></root-icon>
        <root-icon icon="mouse"></root-icon>
        <root-icon icon="rabbit"></root-icon>
        <root-icon icon="bird"></root-icon>
    </div>
    <h2>Factions</h2>
    <div style="display: flex; flex-flow: row wrap;">
        <root-icon icon="marquise"></root-icon>
        <root-icon icon="eyrie"></root-icon>
        <root-icon icon="alliance"></root-icon>
        <root-icon icon="vagabond"></root-icon>
        <root-icon icon="riverfolk"></root-icon>
        <root-icon icon="cult"></root-icon>
        <root-icon icon="duchy"></root-icon>
        <root-icon icon="corvid"></root-icon>
        <root-icon icon="hundreds"></root-icon>
        <root-icon icon="keepers"></root-icon>
    </div>
    <h2>Buildings</h2>
    <div style="display: flex; flex-flow: row wrap;">
        <root-icon icon="ruins"></root-icon>
        <root-icon icon="recruiter"></root-icon>
        <root-icon icon="sawmill"></root-icon>
        <root-icon icon="workshop"></root-icon>
        <root-icon icon="roost"></root-icon>
        <root-icon icon="base"></root-icon>
        <root-icon icon="garden" style="background-color: gray;"></root-icon>
        <root-icon icon="citadel"></root-icon>
        <root-icon icon="market"></root-icon>
        <root-icon icon="stronghold"></root-icon>
        <root-icon icon="waystation"></root-icon>
    </div>
    <h2>Tokens</h2>
    <div style="display: flex; flex-flow: row wrap;">
        <root-icon icon="keep"></root-icon>
        <root-icon icon="wood"></root-icon>
        <root-icon icon="sympathy"></root-icon>
        <root-icon icon="tradePost"></root-icon>
        <root-icon icon="tunnel"></root-icon>
        <root-icon icon="plot"></root-icon>
        <root-icon icon="bomb"></root-icon>
        <root-icon icon="extortion"></root-icon>
        <root-icon icon="raid"></root-icon>
        <root-icon icon="snare"></root-icon>
        <root-icon icon="mob"></root-icon>
        <root-icon icon="figure"></root-icon>
        <root-icon icon="jewelry"></root-icon>
        <root-icon icon="tablet"></root-icon>
    </div>
    <h2>Items</h2>
    <div style="display: flex; flex-flow: row wrap;">
        <root-icon icon="torch"></root-icon>
        <root-icon icon="bag"></root-icon>
        <root-icon icon="boot"></root-icon>
        <root-icon icon="crossbow"></root-icon>
        <root-icon icon="hammer"></root-icon>
        <root-icon icon="sword"></root-icon>
        <root-icon icon="tea"></root-icon>
        <root-icon icon="coins"></root-icon>
    </div>
    <h2>Landmarks</h2>
    <div style="display: flex; flex-flow: row wrap;">
        <root-icon icon="ferry"></root-icon>
        <root-icon icon="tower"></root-icon>
        <root-icon icon="lostCity"></root-icon>
    </div>
    <h2>Actions</h2>
    <div style="display: flex; flex-flow: row wrap;">
        <root-icon icon="upload"></root-icon>
        <root-icon icon="download"></root-icon>
        <root-icon icon="view"></root-icon>
    </div>
`;

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
        :root {
            background-color: var(--${args.backgroundColor}-color);
        }
        root-icon {
            width: ${args.size}px; height: ${args.size}px; color: var(--${args.color}-color);
        }

        root-icon::part(icon) {
            stroke: black; stroke-width: ${args.strokeWidth}; vector-effect: ${args.scalingStroke ? 'none' : 'non-scaling-stroke'}
        }
    </style>
    <root-icon icon="${args.icon}"></root-icon>
    `;

export const Custom = Template.bind({});
Custom.argTypes = {
    size: {
        type: 'number',
        defaultValue: 64,
        control: {
            type: 'range',
            min: 8,
            max: 128,
            step: 8,
        },
    },
    scalingStroke: {
        type: 'boolean',
        defaultValue: true,
    },
    strokeWidth: {
        type: 'number',
        defaultValue: 2,
        control: {
            type: 'range',
            min: 0,
            max: 10,
        },
    },
    icon: {
        options: [
            'marquise',
            'eyrie',
            'alliance',
            'vagabond',
            'riverfolk',
            'lizard',
            'duchy',
            'corvid',
            'hundreds',
            'keepers',
            'bird',
            'fox-wborder',
            'mouse-wborder',
            'rabbit-wborder',
            'recruiter',
            'sawmill',
            'workshop',
            'roost',
            'base',
            'keep',
            'wood',
            'sympathy',
            'torch',
            'bag',
            'boot',
            'crossbow',
            'hammer',
            'sword',
            'tea',
            'coins',
            'ferry',
            'tower',
            'lostCity',
            'ruins',
        ],
        defaultValue: 'marquise',
        control: {
            type: 'select',
        },
    },
    color: {
        type: 'string',
        defaultValue: 'marquise',
        options: [
            'white',
            'marquise',
            'eyrie',
            'alliance',
            'vagabond',
            'lizard',
            'riverfolk',
            'duchy',
            'duchy-alt',
            'corvid',
            'corvid-alt',
            'hundreds',
            'keepers',
            'fox',
            'mouse',
            'rabbit',
            'bird',
            'ferry',
            'tower',
            'lostCity',
        ],
        control: {
            type: 'select',
        },
    },
    backgroundColor: {
        type: 'string',
        defaultValue: 'none',
        options: [
            'none',
            'marquise',
            'eyrie',
            'alliance',
            'vagabond',
            'lizard',
            'riverfolk',
            'duchy',
            'corvid',
            'hundreds',
            'keepers',
            'fox',
            'mouse',
            'rabbit',
            'bird',
            'ferry',
            'tower',
            'lostCity',
        ],
        control: {
            type: 'select',
        },
    }
};
