import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-draft-select';
import '../root.css';

export default {
  title: 'Root/Molecules/DraftSelect',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
    </style>
    <root-draft-select></root-draft-select>
    `;

export const DraftSelect = Template.bind({});
