import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-token';
import '../root.css';

export default {
  title: 'Root/Molecules/Token',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => {
    const token = {type: args.token};
    if (token.type === "tradePost") {
        token.suit = args.suit;
    } else if (token.type === "plot") {
        token.plot = args.plot;
    } else if (token.type === "relic") {
        token.relic = args.relic;
        token.value = args.value;
    }
    return html`
        <style>
        root-token {
            width: ${args.size}px;
            height: ${args.size}px;
        }
        </style>
        <root-token .token="${token}" count="${args.number}"></root-token>
        `;
}

export const Token = Template.bind({});
Token.argTypes = {
    size: {
        type: 'number',
        defaultValue: 64,
        control: {
            type: 'range',
            min: 8,
            max: 128,
            step: 8,
        },
    },
    number: {
        type: 'number',
        defaultValue: 1,
        control: {
            type: 'range',
            min: 1,
            max: 20,
        },
    },
    token: {
        options: ["keep", "wood", "sympathy", "tradePost", "tunnel", "plot", "mob", "relic"],
        control: {
            type: 'select',
        },
    },
    suit: {
        options: ["fox", "mouse", "rabbit"],
        control: "select",
        if: {arg: "token", eq: "tradePost"},
    },
    plot: {
        options: [undefined, "bomb", "extortion", "raid", "snare"],
        control: "select",
        if: {arg: "token", eq: "plot"},
    },
    relic: {
        options: ["figure", "jewelry", "tablet"],
        control: "select",
        if: {arg: "token", eq: "relic"},
    },
    value: {
        options: [undefined, 1, 2, 3],
        control: "select",
        if: {arg: "token", eq: "relic"},
    }
};
