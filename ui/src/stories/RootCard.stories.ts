import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-card';
import '../root.css';

export default {
  title: 'Root/Molecules/Card',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => {
    const card = {deckCard: [args.suit, args.name]};
    return html`
        <style>
        root-card {
            width: ${args.size}px;
            height: ${args.size}px;
        }
        </style>
        <root-card .card="${card}"></root-card>
        `;
}

export const Card = Template.bind({});
Card.argTypes = {
    size: {
        type: 'number',
        defaultValue: 64,
        control: {
            type: 'range',
            min: 8,
            max: 128,
            step: 8,
        },
    },
    suit: {
        options: [null, "bird", "fox", "mouse", "rabbit"],
        defaultValue: null,
        control: {
            type: 'select',
        },
    },
    name: {
        options: [null, "Ambush", "Dominance"],
        defaultValue: null,
        control: {
            type: 'select',
        },
    }
};
