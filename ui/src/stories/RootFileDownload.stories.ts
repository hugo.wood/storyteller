import { Story, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit-html';
import '../root-file-download';
import '../root.css';

export default {
  title: 'Root/Molecules/Download',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
    </style>
    <root-file-download @download="${action("download")}"></root-file-download>
    `;

export const Download = Template.bind({});
