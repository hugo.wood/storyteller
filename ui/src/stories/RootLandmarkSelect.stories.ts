import { Story, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit-html';
import '../root-landmark-select';
import '../root.css';

export default {
  title: 'Root/Molecules/LandmarkSelect',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
    </style>
    <root-landmark-select @change="${action("change")}"></root-landmark-select>
    `;

export const LandmarkSelect = Template.bind({});
