import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-ruins';
import '../root.css';

export default {
  title: 'Root/Molecules/Ruins',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => {
    return html`
        <style>
        root-ruins {
            width: ${args.size}px;
            height: ${args.size}px;
        }
        </style>
        <root-ruins></root-ruins>
        `;
}

export const Ruins = Template.bind({});
Ruins.argTypes = {
    size: {
        type: 'number',
        defaultValue: 64,
        control: {
            type: 'range',
            min: 8,
            max: 128,
            step: 8,
        },
    },
};
