import { Story, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit-html';
import '../root-action-editor';
import '../root.css';

export default {
  title: 'Root/Organisms/ActionEditor',
  component: "root-action-editor",
} as Meta;

let turns = [
    {
        faction: "marquise",
        actions: ["tk->4", "(br,bw)->6", "bs->4", "w->(1,2,4,5,6,7,8,9,10,11,12)", "3#->M"],
    },
    {
        faction: "eyrie",
        actions: ["(b,6w)->3","#despot->$","3#->E"],
    }
];

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
    </style>
    <root-action-editor .turns="${turns}" @change=${action('change')}></root-action-editor>
    `;

export const ActionEditor = Template.bind({});
