import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-building';
import '../root.css';

export default {
  title: 'Root/Molecules/Building',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => {
    const building = {type: args.building};
    if (building.type === "base") {
        building.suit = args.baseSuit;
    } else if (building.type === "garden") {
        building.suit = args.gardenSuit;
    } else if (building.type === "waystation") {
        building.obverse = args.obverse;
        building.reverse = args.reverse;
    }
    return html`
        <style>
        root-building {
            width: ${args.size}px;
            height: ${args.size}px;
        }
        </style>
        <root-building .building="${building}"></root-building>
        `;
}

export const Building = Template.bind({});
Building.argTypes = {
    size: {
        type: 'number',
        defaultValue: 64,
        control: {
            type: 'range',
            min: 8,
            max: 128,
            step: 8,
        },
    },
    building: {
        options: ["recruiter", "sawmill", "workshop", "roost", "base", "garden", "citadel", "market", "stronghold", "waystation"],
        defaultValue: 'recruiter',
        control: {
            type: 'select',
        },
    },
    baseSuit: {
        options: ["fox", "mouse", "rabbit"],
        control: "select",
        if: {arg: "building", eq: "base"},
    },
    gardenSuit: {
        options: ["fox", "mouse", "rabbit"],
        control: "select",
        if: {arg: "building", eq: "garden"},
    },
    obverse: {
        options: ["figure", "jewelry", "tablet"],
        control: "select",
        if: {arg: "building", eq: "waystation"}
    },
    reverse: {
        options: ["figure", "jewelry", "tablet"],
        control: "select",
        if: {arg: "building", eq: "waystation"}
    },
};
