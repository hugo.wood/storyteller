import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-warriors';
import '../root.css';

export default {
  title: 'Root/Molecules/Warriors',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
    root-warriors {
        width: ${args.size}px;
        height: ${args.size}px;
    }
    </style>
    <root-warriors faction="${args.faction}" count="${args.number}" ?warlord="${args.warlord}"></root-warriors>
    `;

export const Warriors = Template.bind({});
Warriors.argTypes = {
    size: {
        type: 'number',
        defaultValue: 64,
        control: {
            type: 'range',
            min: 8,
            max: 128,
            step: 8,
        },
    },
    number: {
        type: 'number',
        defaultValue: 1,
        control: {
            type: 'range',
            min: 1,
            max: 20,
        },
    },
    faction: {
        options: [
            'marquise',
            'eyrie',
            'alliance',
            'vagabond',
            'riverfolk',
            'lizard',
            'duchy',
            'corvid',
            'hundreds',
            'keepers',
        ],
        defaultValue: 'marquise',
        control: {
            type: 'select',
        },
    },
    warlord: {
        defaultValue: false,
        control: "boolean",
        if: { arg: "faction", eq: "hundreds" },
    }
};
