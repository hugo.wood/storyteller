import { Story, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit-html';
import '../root-file-upload';
import '../root.css';

export default {
  title: 'Root/Molecules/Upload',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
    </style>
    <root-file-upload @upload="${action("upload")}"></root-file-upload>
    `;

export const Upload = Template.bind({});
