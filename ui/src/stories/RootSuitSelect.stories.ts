import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-suit-select';
import '../root.css';

export default {
  title: 'Root/Molecules/Suit Select',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
    root-building {
        width: 800;
        height: 600;
    }
    </style>
    <root-suit-select></root-suit-select>
    `;

export const Example = Template.bind({});
