import { Story, Meta } from '@storybook/web-components';
import { action } from '@storybook/addon-actions';
import { html } from 'lit-html';
import '../root-supply';
import '../root.css';

export default {
  title: 'Root/Organisms/Supply',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => {
    const quests = args.quests ? [
        {piece: {card: {quest: ["fox", "Errand"]}}, count: 1},
        {piece: {card: {quest: ["rabbit", "Escort"]}}, count: 1},
        {piece: {card: {quest: ["mouse", "LogisticsHelp"]}}, count: 1},
    ] : [];
    const piecesByLocation = [
        {location: {supply: null}, pieces: [
            {piece: {item: ["bag", "refreshed"]}, count: args.bag},
            {piece: {item: ["boot", "refreshed"]}, count: args.boot},
            {piece: {item: ["crossbow", "refreshed"]}, count: args.crossbow},
            {piece: {item: ["hammer", "refreshed"]}, count: args.hammer},
            {piece: {item: ["sword", "refreshed"]}, count: args.sword},
            {piece: {item: ["tea", "refreshed"]}, count: args.tea},
            {piece: {item: ["coins", "refreshed"]}, count: args.coins},
        ]},
        {location: "drawPile", pieces: [{piece: {card: {deckCard: [null, null]}}, count: args.drawPile}]},
        {location: "discard", pieces: [
            {piece: {card: {deckCard: ["bird", "BoatBuilders"]}}, count: 1},
            {piece: {card: {deckCard: ["bird", "CorvidPlanners"]}}, count: 1},
            {piece: {card: {deckCard: ["mouse", "LeagueOfAdventurousMice"]}}, count: 1},
            {piece: {card: {deckCard: ["fox", "Ambush"]}}, count: 1},
        ]},
        {location: "availableDominanceCards", pieces: [
            {piece: {card: {deckCard: ["fox", "Dominance"]}}, count: 1},
            {piece: {card: {deckCard: ["mouse", "Dominance"]}}, count: 1},
        ]},
        {location: "availableQuests", pieces: quests},
    ];
    return html`
    <style>
    </style>
    <root-supply .piecesByLocation="${piecesByLocation}"></root-supply>
    `;
}

export const Supply = Template.bind({});
Supply.argTypes = {
    drawPile: {
        type: "number",
        defaultValue: 54,
        control: {
            type: "range",
            min: 0,
            max: 54,
        }
    },
    quests: {
        defaultValue: true,
        control: "boolean",
    },
    bag: {
        type: 'number',
        defaultValue: 2,
        control: {
            type: 'range',
            min: 0,
            max: 2,
        },
    },
    boot: {
        type: 'number',
        defaultValue: 2,
        control: {
            type: 'range',
            min: 0,
            max: 2,
        },
    },
    coins: {
        type: 'number',
        defaultValue: 2,
        control: {
            type: 'range',
            min: 0,
            max: 2,
        },
    },
    crossbow: {
        type: 'number',
        defaultValue: 1,
        control: {
            type: 'range',
            min: 0,
            max: 1,
        },
    },
    hammer: {
        type: 'number',
        defaultValue: 1,
        control: {
            type: 'range',
            min: 0,
            max: 1,
        },
    },
    sword: {
        type: 'number',
        defaultValue: 2,
        control: {
            type: 'range',
            min: 0,
            max: 2,
        },
    },
    tea: {
        type: 'number',
        defaultValue: 2,
        control: {
            type: 'range',
            min: 0,
            max: 2,
        },
    },
}
