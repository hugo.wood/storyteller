import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-forest';
import '../root.css';

export default {
  title: 'Root/Organisms/Forest',
} as Meta;

const pieces = [
    {piece: {pawn: "vagabond1"}, count: 1},
    {piece: {token: {type: "relic", relic: "jewelry"}}, count: 1},
]

const Template: Story<Partial<PageProps>> = (args) => html`
    <style>
        root-forest {
            width: ${args.size}px; height: ${args.size}px;
        }
    </style>
    <root-forest .pieces="${pieces}"></root-forest>
    `;

export const Forest = Template.bind({});
Forest.argTypes = {
    size: {
        type: 'number',
        defaultValue: 128,
        control: {
            type: 'range',
            min: 8,
            max: 128,
            step: 8,
        },
    },
    strokeWidth: {
        type: 'number',
        defaultValue: 2,
        control: {
            type: 'range',
            min: 1,
            max: 10,
        },
    },
};
