import { Story, Meta } from '@storybook/web-components';
import { html } from 'lit-html';
import '../root-item';
import '../root.css';

export default {
  title: 'Root/Molecules/Item',
} as Meta;

const Template: Story<Partial<PageProps>> = (args) => {
    const item = [args.item, args.state];
    return html`
        <style>
        root-item {
            width: ${args.size}px;
            height: ${args.size}px;
        }
        </style>
        <root-item .item="${item}" ?damaged="${args.damaged}"></root-item>
        `;
}

export const Item = Template.bind({});
Item.argTypes = {
    size: {
        type: 'number',
        defaultValue: 64,
        control: {
            type: 'range',
            min: 8,
            max: 128,
            step: 8,
        },
    },
    item: {
        options: ["bag", "boot", "coins", "crossbow", "hammer", "sword", "tea", "torch"],
        defaultValue: "bag",
        control: {
            type: 'select',
        },
    },
    state: {
        options: ["refreshed", "exhausted"],
        defaultValue: "refreshed",
        control: {
            type: 'select',
        },
    },
    damaged: {
        control: "boolean",
    }
};
