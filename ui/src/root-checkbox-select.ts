import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';

@customElement('root-checkbox-select')
export class RootCheckboxSelect extends LitElement {
    @property()
    name: string = "";
    @property()
    options: object = {};

    override render() {
        return html`
            ${map(Object.entries(this.options), ([value, label]) => html`
                <input type="checkbox" name="${this.name}" id="${value}" value="${value}" /><label for="${value}">${label}</label>
            `)}
        `;
    }
}
