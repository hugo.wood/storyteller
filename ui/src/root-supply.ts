import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {when} from 'lit/directives/when.js';
import {LocatedPieces} from './types';
import './root-item';
import './root-card';

@customElement('root-supply')
export class RootSupply extends LitElement {
    @property()
    piecesByLocation: LocatedPieces[] = [];

    override render() {
        const items = this.piecesByLocation
            .find(({location}) => typeof location != "string" && location.supply === null)
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.item) {
                    return new Array(count).fill(piece.item);
                } else {
                    return [];
                }
            })
            ?.sort((i1, i2) => i1[0].localeCompare(i2[0]))
            || [];
        const drawPileCount = this.piecesByLocation.find(({location}) => location == "drawPile")?.pieces?.[0]?.count || 0;
        const discard = this.piecesByLocation
            .find(({location}) => location == "discard")
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            || [];
        const availableDominanceCards = this.piecesByLocation
            .find(({location}) => location == "availableDominanceCards")
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            || [];
        const availableQuests = this.piecesByLocation
            .find(({location}) => location == "availableQuests")
            ?.pieces
            ?.flatMap(({piece, count}) => {
                if (piece.card) {
                    return new Array(count).fill(piece.card);
                } else {
                    return [];
                }
            })
            || [];

        return html`
            <style>
                :host {
                    display: block;
                }
                div.container {
                    width: calc(100% - 16px);
                    background-color: lightgray;
                    display: flex;
                    flex-direction: column;
                    gap: 8px;
                    padding: 8px;
                }
                div.component {
                    display: flex;
                    flex-direction: row;
                    flex-wrap: wrap;
                }
                root-card, root-item {
                    height: 1.5em;
                }
            </style>
            <div class="container">
                <div class="component">${drawPileCount}×<root-card></root-card></div>
                <div class="component">D: ${map(discard, (card) => html`<root-card .card="${card}"></root-card>`)}</div>
                <div class="component">Dom: ${map(availableDominanceCards, (card) => html`<root-card .card="${card}"></root-card>`)}</div>
                <div class="component">
                    ${map(items, (item) => html`<root-item .item="${item}"></root-item>`)}
                </div>

                ${when(availableQuests.length, () => html`
                <div class="component">
                    Q: ${map(availableQuests, (quest) => html`<root-card .card="${quest}"></root-card>`)}
                </div>
                `)}
            </div>
            `;
    }
}
