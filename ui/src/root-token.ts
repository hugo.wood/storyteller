import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {Token} from './types';
import './root-icon';

@customElement('root-token')
export class RootToken extends LitElement {
    @property()
    token!: Token;

    @property()
    count: number = 1;

    renderCount() {
        if (this.count > 1) {
            return html`
            <svg class="decoration" viewBox="0 0 10 10">
                <text x="5" y="10">${this.count}</text>
            </svg>
            `;
        } else {
            return null;
        }
    }

    renderDecorations() {
        if (this.token.type === "tradePost") {
            return html`
            <root-icon class="${this.token.type}-decoration" icon="${this.token.suit}-wborder"></root-icon>
            `;
        } else if (this.token.value) {
            return html`
            <svg class="relic-decoration" viewBox="0 0 10 10">
                <text x="5" y="10">${this.token.value}</text>
            </svg>
            `;
        } else {
            return null;
        }
    }

    override render() {
        let icon = this.token.type;
        if (this.token.plot) {
            icon = this.token.plot;
        } else if (this.token.relic) {
            icon = this.token.relic;
        }
        return html`
            <style>
                :host {
                    display: block;
                    width: 100%;
                    height: 100%;
                }
                div {
                    position: relative; width: 100%; height: 100%;
                }
                .decoration {
                    position: absolute; top: 50%; left: 50%; translate: -50% -50%; width: 50%; height: 50%; z-index: 2;
                }
                .decoration text {
                    font: bold 7pt Arial;
                    fill: #fff;
                    text-anchor: middle;
                }
                .tradePost-decoration {
                    position: absolute;
                    top: 45%;
                    left: 50%;
                    width: 35%;
                    height: 35%;
                    translate: -50% -50%;
                    z-index: 2;
                    color: var(--${this.token.suit}-color);
                }
                .relic-decoration {
                    position: absolute;
                    top: 35%;
                    left: 75%;
                    width: 60%;
                    height: 60%;
                    translate: -50% -50%;
                    z-index: 2;
                    color: black;
                }
                .relic-decoration text {
                    font: bold 7pt Arial;
                    fill: #000;
                    text-anchor: middle;
                }
                .cardboard {
                    width: 100%; height: 100%; position: absolute; top: 0; left: 0;
                }
                .cardboard circle {
                    fill: var(--${this._faction()}-color);
                }
                root-icon {
                    position: absolute; top: 50%; left: 50%; translate: -50% -50%; width: 90%; height: 90%; z-index: 1; color: white;
                }
            </style>
            <div>
                <svg class="cardboard">
                    <circle cx="50%" cy="50%" r="50%"/>
                </svg>
                <root-icon icon="${icon}"></root-icon>
                ${this.renderCount()}
                ${this.renderDecorations()}
            </div>
            `;
    }

    _faction() {
        switch (this.token.type) {
            case "keep": case "wood":
                return "marquise";
            case "sympathy":
                return "alliance";
            case "tradePost":
                return "riverfolk";
            case "tunnel":
                return "duchy";
            case "plot":
                return "corvid";
            case "mob":
                return "hundreds";
            case "relic":
                return "keepers";
        }
        return null;
    }
}
