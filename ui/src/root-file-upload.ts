import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import './root-icon';

@customElement('root-file-upload')
export class RootFileUpload extends LitElement {
    @property()
    content?: string;

    override render() {
        return html`
            <style>
                :host {
                    display: block;
                    width: 100%;
                    height: 100%;
                }
                root-icon {
                    width: 2em;
                    height: 2em;
                }
            </style>
            <div style="display: flex; flex-direction: row; gap: 10px">
                <input type="file" id="dropZone" @change="${this._onChange}"/>
                <button @click="${this._onClick}"><root-icon icon="upload"></root-icon></button>
            </div>
            `;
    }

    private async _onChange(event: Event) {
        const files = (event.target as HTMLInputElement).files;
        if (files && files[0]) {
            this.content = await files[0].text();
        }
    }

    private _onClick() {
        this.dispatchEvent(new Event("upload"));
    }
}
