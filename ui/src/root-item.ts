import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {when} from 'lit/directives/when.js';
import {Item} from './types';
import './root-icon';

@customElement('root-item')
export class RootItem extends LitElement {
    @property()
    item!: Item;

    @property({type: Boolean})
    damaged: boolean = false;

    override render() {
        return html`
            <style>
                :host {
                    display: block;
                    aspect-ratio: 1 / 1;
                }
                div {
                    position: relative; width: 100%; height: 100%;
                }
                .cardboard {
                    position: absolute; top: 0; left: 0; width: 100%; height: 100%;
                }
                .cardboard rect {
                    stroke: black;
                    stroke-width: 2px;
                    fill: ${this._color()};
                    vector-effect: non-scaling-stroke;
                }
                .damage {
                    position: absolute; top: 0; left: 0; width: 100%; height: 100%;
                    z-index: 2;
                }
                .damage path {
                    stroke: red;
                    stroke-width: 2px;
                    vector-effect: non-scaling-stroke;
                }
                .icon {
                    position: absolute;
                    top: 5%;
                    left: 5%;
                    width: 90%;
                    height: 90%;
                    z-index: 1;
                    color: black;
                }
            </style>
            <div>
                <svg class="cardboard">
                    <rect x="0" y="0" width="100%" height="100%" rx="15%"/>
                </svg>
                <root-icon class="icon" icon="${this.item[0]}"></root-icon>
                ${when(this.damaged, () => html`<svg class="damage" viewBox="0 0 1 1"><path d="M0,0 L1,1 M1,0 L0,1"/></svg>`)}
            </div>
            `;
    }

    private _color() {
        switch (this.item[1]) {
            case "exhausted":
                return "lightgray";
            default:
                return "white";
        }
        return null;
    }
}
