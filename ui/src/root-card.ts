import {LitElement, html, svg} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {when} from 'lit/directives/when.js';
import {Card} from './types';
import './root-icon';

@customElement('root-card')
export class RootCard extends LitElement {
    @property({type: Object})
    card?: Card;

    override render() {
        const suit = this._suit();
        return html`
            <style>
                :host {
                    display: inline-block;
                    aspect-ratio: 25 / 35;
                    /* width: 100%;
                    height: 100%; */
                }
                div {
                    position: relative;
                    width: 100%; height: 100%;
                }
                .card {
                    position: absolute; top: 0; left: 0; width: 100%; height: 100%;
                }
                .card rect.border {
                    stroke: black;
                    stroke-width: 1px;
                    fill: ${this._color()};
                }
                .icon {
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    width: 60%;
                    height: 60%;
                    translate: -50% -50%;
                    z-index: 1;
                    color: white;
                }
            </style>
            <div title="${this._name()}">
                <svg class="card" viewBox="0 0 25 35">
                    <rect class="border" x="0" y="0" width="25" height="35" rx="3"/>
                    ${when(!suit, () => svg`
                        <rect x="2.5" y="2.5" width="10" height="15" style="fill: var(--fox-color)" />
                        <rect x="12.5" y="2.5" width="10" height="15" style="fill: var(--rabbit-color)" />
                        <rect x="2.5" y="17.5" width="10" height="15" style="fill: var(--mouse-color)" />
                        <rect x="12.5" y="17.5" width="10" height="15" style="fill: var(--bird-color)" />
                        `)}
                </svg>
                ${when(suit, () => html`
                    <root-icon class="icon" icon="${suit}"></root-icon>
                `)}
            </div>
            `;
    }

    private _color() {
        const suit = this._suit();
        if (suit) {
            return `var(--${suit}-color)`;
        } else {
            return "white";
        }
    }

    private _suit() {
        if (this.card?.duchyMinister) {
            return "duchy";
        } else if (this.card?.eyrieCard || this.card?.keepersCard) {
            return "bird";
        } else {
            return this.card?.deckCard?.[0] || this.card?.quest?.[0];
        }
    }

    private _name() {
        return this.card?.deckCard?.[1]
            || this.card?.eyrieCard
            || this.card?.quest?.[1]
            || this.card?.duchyMinister
            || this.card?.keepersCard;
    }
}
