import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {when} from 'lit/directives/when.js';
import './root-icon';

@customElement('root-warriors')
export class RootWarriors extends LitElement {
    @property()
    faction: string = "";

    @property({type: Number})
    count: number = 1;

    @property({type: Boolean})
    warlord: boolean = false;

    override render() {
        return html`
            <style>
                :host {
                    display: block;
                    width: 100%;
                    height: 100%;
                }
                root-icon {
                    color: var(--${this.faction}-color);
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                }
                root-icon::part(icon) {
                    stroke: #000;
                    stroke-width: 4px;
                }
                text {
                    font: bold 7pt Arial;
                    fill: #fff;
                    text-anchor: middle;
                }
                .banner {
                    position: absolute;
                    top: -25%;
                    left: 20%;
                    width: 80%;
                    height: 80%;
                }
                .banner path {
                    color: var(--${this.faction}-color);
                    stroke: #000;
                    stroke-width: 0.5px;
                }
            </style>
            <div style="position: relative; width: 100%; height: 100%">
                ${when(this.warlord, () => html`
                    <svg class="banner" xmlns="http://www.w3.org/2000/svg" width="17.415" height="17.941" viewBox="0 0 4.608 4.747"><path style="fill:currentColor;" d="M105.74 329.157s-4.372.071-6.238 3.318c-1.865 3.248-5.7 0-5.7 0H92.68v-12.012h1.486v6.788s2.223 2.471 5.098 0c2.876-2.47 5.228.535 5.228.535z" transform="matrix(.35278 0 0 -.35278 -32.695 117.8)"/></svg>
                `)}
                <root-icon icon="${this.faction}"></root-icon>
                ${this.renderCount()}
            </div>
            `;
    }

    renderCount() {
        if (!this.faction.startsWith("vagabond")) {
            return html`
            <svg viewBox="0 0 10 10" style="position: absolute; top: 55%; left: 50%; translate: -50% -50%; width: 47%; height: 47%;">
                <text x="5" y="10">${this.count + Number(this.warlord)}</text>
            </svg>
            `;
        } else {
            return null;
        }
    }
}
