import {LitElement, html, css, unsafeCSS} from 'lit';
import {customElement, property, queryAll} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import './root-icon';
import {Landmark} from './types';

@customElement('root-landmark-select')
export class RootLandmarkSelect extends LitElement {
    static readonly landmarks = ["ferry", "tower", "lostCity"];

    @property()
    value: Landmark[] = [];

    @queryAll("input")
    _checkboxes!: NodeListOf<HTMLInputElement>

    override render() {
        return html`
            <style>
            :host {
                display: inline-block;
            }
            root-icon {
                width: 32px;
                height: 32px;
                color: lightgray;
                background-color: white;
            }
            input {
                display: none;
            }
            label {
                display: block;
            }
            ${map(RootLandmarkSelect.landmarks, (landmark) => css`
            input:checked + label > root-icon[icon="${unsafeCSS(landmark)}"] {
                color: var(--${unsafeCSS(landmark)}-color);
            }
            `)}
            div {
                display: flex;
                flex-direction: row;
            }
            </style>
            <div>
            ${map(RootLandmarkSelect.landmarks, (landmark) => html`
                <input type="checkbox" name="landmark" id="${landmark}" value="${landmark}" ?checked="${this.value !== undefined && this.value.includes(landmark)}" @change="${this._onChange}" />
                <label for="${landmark}">
                    <root-icon icon="${landmark}"></root-icon>
                </label>
            `)}
            <div>
        `;
    }

    private _onChange() {
        this.value = Array.from(this._checkboxes).filter(input => input.checked).map(input => input.value);
        this.dispatchEvent(new Event("change"));
    }
}
