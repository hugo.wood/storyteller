import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {Faction} from './types';
import './root-icon';

@customElement('root-faction-select')
export class RootFactionSelect extends LitElement {
    static readonly factions = {
        "marquise": "Marquise",
        "eyrie": "Eyrie",
        "alliance": "Alliance",
        "vagabond1": "Vagabond 1",
        "vagabond2": "Vagabond 2",
        "cult": "Cult",
        "riverfolk": "Riverfolk",
        "duchy": "Duchy",
        "corvid": "Corvid",
        "hundreds": "Hundreds",
        "keepers": "Keepers",
    };

    @property()
    faction?: Faction;

    override render() {
        return html`
            <style>
            :host {
                display: inline-block;
            }
            </style>
            <select @change="${this._onChange}">
                <option ?selected="${this.faction === undefined}"></option>
                ${map(Object.entries(RootFactionSelect.factions), ([faction, label]) => html`
                    <option value="${faction}" ?selected="${faction === this.faction}">${label}</option>
                    `)}
            </select>
        `;
    }

    private _onChange(event: Event) {
        const faction = (event.target as HTMLSelectElement).value;
        if (!faction) {
            this.faction = undefined;
        } else {
            this.faction = faction;
        }
        this.dispatchEvent(new Event("change"));
    }
}
