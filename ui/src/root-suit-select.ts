import {LitElement, html, css, unsafeCSS} from 'lit';
import {customElement} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import './root-icon';

@customElement('root-suit-select')
export class RootSuitSelect extends LitElement {
    override render() {
        return html`
            <style>
            root-icon {
                width: 32px;
                height: 32px;
                color: gray;
                background-color: white;
            }
            input {
                display: none;
            }
            label {
                display: inline-block;
            }
            ${map(["fox", "mouse", "rabbit"], (suit) => css`
            input:checked + label > root-icon[icon="${unsafeCSS(suit)}-wborder"] {
                color: var(--${unsafeCSS(suit)}-color);
            }
            `)}
            </style>
            ${map(["fox", "mouse", "rabbit"], (suit) => html`
                <input type="radio" name="suit" id="${suit}" value="${suit}" />
                <label for="${suit}">
                    <root-icon icon="${suit}-wborder"></root-icon>
                </label>
            `)}
        `;
    }
}
