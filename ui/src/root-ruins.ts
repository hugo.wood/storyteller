import {LitElement, html} from 'lit';
import {customElement} from 'lit/decorators.js';
import './root-icon';

@customElement('root-ruins')
export class RootRuins extends LitElement {
    override render() {
        return html`
            <style>
                :host {
                    display: block;
                    width: 100%;
                    height: 100%;
                }
                div {
                    position: relative; width: 100%; height: 100%;
                }
                .icon {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 1;
                    clip-path: inset(0% round 15%)
                }
            </style>
            <div>
                <root-icon class="icon" icon="ruins"></root-icon>
            </div>
            `;
    }
}
