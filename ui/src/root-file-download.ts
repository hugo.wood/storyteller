import {LitElement, html} from 'lit';
import {customElement} from 'lit/decorators.js';
import './root-icon';

@customElement('root-file-download')
export class RootFileDownload extends LitElement {
    override render() {
        return html`
            <style>
                :host {
                    display: block;
                    width: 100%;
                    height: 100%;
                }
                root-icon {
                    width: 2em;
                    height: 2em;
                }
            </style>
            <button @click="${this._onClick}"><root-icon icon="download"></root-icon></button>
            `;
    }

    private _onClick() {
        this.dispatchEvent(new Event("download"));
    }
}
