import {LitElement, html, css, unsafeCSS} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import './root-icon';

@customElement('root-draft-select')
export class RootDraftSelect extends LitElement {
    draftOptions = ["marquise", "eyrie", "alliance", "vagabond", "cult", "riverfolk", "duchy", "corvid", "hundreds", "keepers"];
    characters = ["adventurer", "arbiter", "harrier", "ranger", "ronin", "scoundrel", "thief", "tinker", "vagrant"];

    @property()
    value?: string[] = [];

    override render() {
        return html`
            <style>
            :host {
                display: inline-block;
            }
            root-icon {
                width: 32px;
                height: 32px;
                color: lightgray;
                background-color: white;
            }
            input {
                display: none;
            }
            label {
                display: block;
            }
            ${map(this.draftOptions, (faction) => css`
            input:checked + label > root-icon[icon="${unsafeCSS(faction)}"] {
                color: var(--${unsafeCSS(faction)}-color);
            }
            `)}
            div {
                display: flex;
                flex-direction: row;
            }
            </style>
            <div>
            ${map(this.draftOptions, (faction) => html`
                <input type="checkbox" name="faction" id="${faction}" value="${faction}" ?checked="${this.value && this.value.includes(faction)}" />
                <label for="${faction}">
                    <root-icon icon="${faction}"></root-icon>
                </label>
                ${this.renderCharacterSelector(faction)}
            `)}
            <div>
        `;
    }

    renderCharacterSelector(faction: string) {
        if (faction === "vagabond") {
            return html`
            <select name="character">
                <option></option>
                ${map(this.characters, (character) => html`
                    <option value="${character}" ?selected="${this.value && this.value.includes(character)}">${character}</option>
                    `)}
            </select>
            `;
        } else {
            return null;
        }
    }
}
