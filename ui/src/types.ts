export type Faction = string

export interface TurnText {
    faction?: string,
    actions: string[],
}

export interface Building {
    type: string,
    suit?: string,
    obverse?: string,
    reverse?: string,
}

export interface Token {
    type: string,
    suit?: string,
    plot?: string|null,
    relic?: string,
    value?: number|null,
}

export type Warrior = string;

export interface Pawn {
    faction: Faction,
}

export type Item = string[];

export interface Card {
    deckCard?: (string|null)[],
    eyrieLeader?: string,
    eyrieCard?: string,
    vagabondCharacter?: string,
    quest?: string[],
    duchyMinister: string,
    hundredsMood?: string,
    keepersCard?: string,
}

export type Landmark = string;

export type Marker = string;

export interface Piece {
    building?: Building,
    token?: Token,
    warrior?: Warrior,
    hundredsPiece?: string,
    pawn?: Pawn,
    item?: Item,
    card?: Card,
    landmark?: Landmark,
    marker?: Marker,
}

export interface Quantity {
    piece: Piece,
    count: number,
}

export type Location = string | {
    clearing?: number,
    path?: number[],
    forest?: number[],
    supply?: Faction|null,
    crafted?: Faction,
    hand?: Faction,
    eyrieDecree?: string,
    vagabondCharacter?: number,
    vagabondTrack?: number,
    vagabondSatchel?: number,
    vagabondDamaged?: number,
    vagabondCompletedQuests?: number,
    riverfolkCommitted?: string|null,
    keepersRetinue?: string,
}

export interface LocatedPieces {
    location: Location,
    pieces: Quantity[],
}

export interface Clearing {
    suit: string,
    buildingSlotCount: number,
    x: number,
    y: number,
}

export interface Forest {
    forest: number[],
    x: number,
    y: number,
}

export interface Map {
    clearings: Clearing[],
    forests: Forest[],
    paths: number[][],
    rivers: number[][],
    lakes: number[][],
}

export interface TurnRecord {
    faction: Faction,
    actions: any[],
}

export interface PlayerRecord {
    name?: string,
    faction?: Faction,
}

export interface GameRecord {
    mapName: string,
    deckName: string,
    clearingSuits?: string[],
    landmarks: Landmark[],
    draftPool?: string[],
    players?: PlayerRecord[],
    turns: TurnRecord[],
}

export interface State {
    piecesByLocation: LocatedPieces[],
    scores: any,
}

export interface Turn {
    faction: Faction,
    states: State[],
}
