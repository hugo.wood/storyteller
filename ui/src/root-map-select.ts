import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import maps from './root-maps';

@customElement('root-map-select')
export class RootMapSelect extends LitElement {
    @property()
    value: string = "autumn";

    override render() {
        return html`
            <style>
            :host {
                display: inline-block;
            }
            div {
                display: flex;
                flex-direction: row;
            }
            </style>
            <div>
            <select name="mapName" @change="${this._onChange}">
                ${map(Object.keys(maps), (mapName) => html`
                    <option value="${mapName}" ?selected="${mapName == this.value}">${mapName}</option>
                    `)}
            </select>
            <div>
        `;
    }

    private _onChange(event: Event) {
        this.value = (event.target as HTMLSelectElement).value;
        this.dispatchEvent(new Event("change"));
    }
}
