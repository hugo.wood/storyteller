import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {map} from 'lit/directives/map.js';
import {produce} from 'immer';
import {GameRecord, TurnText, State} from './types';
import './root-icon';
import {RootActionEditor} from './root-action-editor';
import './root-action-editor';
import {RootPlayerEditor} from './root-player-editor';
import './root-player-editor';
import './root-player-board';
import {RootMapSelect} from './root-map-select';
import './root-map-select';
import {RootDeckSelect} from './root-deck-select';
import './root-deck-select';
import {RootLandmarkSelect} from './root-landmark-select';
import './root-landmark-select';
import './root-supply';
import './root-map';
import maps from './root-maps';

@customElement('root-storyteller')
export class RootStoryteller extends LitElement {
    @property()
    gameRecord: GameRecord = {
        mapName: "autumn",
        deckName: "enp",
        landmarks: [],
        turns: [],
    };

    @property()
    turns: TurnText[] = [];

    @property()
    state: State = {piecesByLocation: [], scores: {}};

    override render() {
        return html`
            <style>
            :host {
                display: block;
            }

            .screen {
                display: grid;
                width: 100%;
                height: 100%;
                grid: "menu menu menu" 100px "map status action" auto / auto 300px 250px;
                column-gap: 20px;
            }

            root-map {
                grid-area: map;
            }

            .menu {
                grid-area: menu
            }

            .menu div {
                display: flex;
                flex-direction: row;
                gap: 20px;
            }

            .status {
                grid-area: status;
            }
            .status div {
                display: flex;
                flex-direction: column;
                gap: 8px;
            }
            .status root-icon {
                width: 2em;
                height: 2em;
            }
            .action {
                grid-area: action;
            }
            root-supply, root-player-board {
                width: 100%;
            }
            root-action-editor {
                height: 100%;
                max-height: 80vh;
                overflow-y: scroll;
            }
            </style>
            <div class="screen">
                <div class="menu">
                    <div>
                    <h1>Storyteller</h1>
                    <slot name="tools"></slot>
                    <p>Map <root-map-select value="${this.gameRecord.mapName}" @change="${this._onMapChange}"></root-map-select></p>
                    <p>Deck <root-deck-select value="${this.gameRecord.deckName}" @change="${this._onDeckChange}"></root-deck-select></p>
                    <p>Landmarks <root-landmark-select .value="${this.gameRecord.landmarks}" @change="${this._onLandmarksChange}"></root-landmark-select></p>
                    </div>
                </div>
                <root-map .map="${maps[this.gameRecord.mapName]}" .clearingSuits="${this.gameRecord.clearingSuits}" .pieces="${this.state.piecesByLocation}"></root-map>
                <div class="status">
                    <div>
                        <root-supply .piecesByLocation="${this.state.piecesByLocation}"></root-supply>
                        ${map(this.gameRecord.players, (player, i) => html`
                            <div style="display: flex; gap: 0; flex-direction: column;">
                                <root-player-editor faction="${player.faction}" name="${player.name}" .score="${this.playerScore(player.faction)}" @change="${(e: Event) => this._onPlayerChange(e, i)}">
                                    <button @click="${() => this.removePlayer(i)}">x</button>
                                </root-player-editor>
                                <root-player-board faction="${player.faction}" .piecesByLocation="${this.state.piecesByLocation}"></root-player-board>
                            </div>
                            `)}
                        <button @click="${this.addPlayer}">+player</button>
                    </div>
                </div>
                <root-action-editor class="action" .turnOrder="${this.gameRecord.players?.map(player => player.faction)}" .turns="${this.turns}" @change="${this._onActionChange}"></root-action-editor>
            </div>
        `;
    }

    playerScore(faction?: string): number|undefined {
        if (faction !== undefined) {
            return this.state.scores[faction];
        } else {
            return undefined;
        }
    }

    private addPlayer() {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            if (!draft.players) {
                draft.players = [];
            }
            draft.players.push({});
        });
        this.dispatchEvent(new Event("change"));
    }

    private removePlayer(i: number) {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            draft.players?.splice(i, 1);
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onPlayerChange(event: Event, i: number) {
        const target = event.target as RootPlayerEditor;
        this.gameRecord = produce(this.gameRecord, (draft) => {
            if (draft.players) {
                draft.players[i] = {
                    name: target.name,
                    faction: target.faction,
                };
            }
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onMapChange(event: Event) {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            draft.mapName = (event.target as RootMapSelect).value;
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onDeckChange(event: Event) {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            draft.deckName = (event.target as RootDeckSelect).value;
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onLandmarksChange(event: Event) {
        this.gameRecord = produce(this.gameRecord, (draft) => {
            draft.landmarks = (event.target as RootLandmarkSelect).value;
        });
        this.dispatchEvent(new Event("change"));
    }

    private _onActionChange(event: Event) {
        this.turns = (event.target as RootActionEditor).turns;
        this.dispatchEvent(new Event("change"));
    }
}
