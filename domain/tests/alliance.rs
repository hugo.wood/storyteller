use rstest::rstest;
use storyteller_domain::{
    action::{Action, Pieces},
    piece::{Building, Card, CardName, CardSuit, Faction, Location, Piece, Suit, Token},
};

#[rstest]
fn setup_partisans() {
    let action = Action::parse(Faction::Alliance, "3#->$").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Card(Card::DeckCard(None, None)), 3)],
        at: vec![Location::AllianceSupporters],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn spend_sympathy() {
    let action =
        Action::parse(Faction::Alliance, "(#bboat,#fa,#ba)$->").expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![
            (
                Piece::Card(Card::DeckCard(
                    Some(CardSuit::Bird),
                    Some(CardName::BoatBuilders),
                )),
                1,
            ),
            (
                Piece::Card(Card::DeckCard(Some(CardSuit::Fox), Some(CardName::Ambush))),
                1,
            ),
            (
                Piece::Card(Card::DeckCard(Some(CardSuit::Bird), Some(CardName::Ambush))),
                1,
            ),
        ]),
        from: Location::AllianceSupporters,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn place_sympathy() {
    let action = Action::parse(Faction::Alliance, "t->1").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Token(Token::Sympathy), 1)],
        at: vec![Location::Clearing(1)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn place_base() {
    let action = Action::parse(Faction::Alliance, "bm->5").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Building(Building::Base { suit: Suit::Mouse }), 1)],
        at: vec![Location::Clearing(5)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn train_officer() {
    let action = Action::parse(Faction::Alliance, "w->$o").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Warrior(Faction::Alliance), 1)],
        at: vec![Location::AllianceOfficers],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn lose_officers() {
    let action = Action::parse(Faction::Alliance, "2w$o->").expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Alliance), 2)]),
        from: Location::AllianceOfficers,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn mobilize() {
    let action = Action::parse(Faction::Alliance, "#A->$").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Card(Card::DeckCard(None, None)), 1)]),
        from: Location::Hand(Faction::Alliance),
        to: Location::AllianceSupporters,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn gain_sympathy() {
    let action = Action::parse(Faction::Marquise, "#M->A$").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Card(Card::DeckCard(None, None)), 1)]),
        from: Location::Hand(Faction::Marquise),
        to: Location::AllianceSupporters,
        commentary: None,
    };

    assert_eq!(expected, action);
}
