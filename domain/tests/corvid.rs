use rstest::rstest;
use storyteller_domain::{
    action::Action,
    piece::{Card, Faction, Location, Piece, Plot, Token},
    state::State,
};

#[rstest]
fn setup_plot_and_warrior() {
    let action = Action::parse(Faction::Corvid, "(2w,t)->5").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![
            (Piece::Warrior(Faction::Corvid), 2),
            (Piece::Token(Token::Plot { plot: None }), 1),
        ],
        at: vec![Location::Clearing(5)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn reveal_card_for_exposure() {
    let action = Action::parse(Faction::Duchy, "#^C").expect("couldn't parse action");

    let expected = Action::Reveal {
        faction: Faction::Duchy,
        pieces: Some(vec![(Piece::Card(Card::DeckCard(None, None)), 1)]),
        to: Some(Faction::Corvid),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn flip() {
    let action = Action::parse(Faction::Corvid, "t5^tb").expect("couldn't parse action");

    let expected = Action::Flip {
        faction: Faction::Corvid,
        at: Location::Clearing(5),
        obverse: Piece::Token(Token::Plot { plot: None }),
        reverse: Piece::Token(Token::Plot {
            plot: Some(Plot::Bomb),
        }),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn trick_face_down() {
    let action = Action::parse(Faction::Corvid, "t5<->t3").expect("couldn't parse action");

    let expected = Action::Swap {
        pieces: [
            Piece::Token(Token::Plot { plot: None }),
            Piece::Token(Token::Plot { plot: None }),
        ],
        locations: [Location::Clearing(5), Location::Clearing(3)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn removed_plot_becomes_secret() {
    let action = Action::parse(Faction::Corvid, "tb5->").expect("couldn't parse action");

    let state = State::builder()
        .faction(Faction::Corvid)
        .supply(Faction::Corvid, Piece::Token(Token::Plot { plot: None }), 7)
        .clearing(
            5,
            vec![(
                Piece::Token(Token::Plot {
                    plot: Some(Plot::Bomb),
                }),
                1,
            )],
        )
        .build();

    let expected = State::builder()
        .faction(Faction::Corvid)
        .supply(Faction::Corvid, Piece::Token(Token::Plot { plot: None }), 8)
        .clearing(5, vec![])
        .build();

    assert_eq!(expected, state.act(&action));
}
