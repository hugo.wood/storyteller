use rstest::rstest;
use storyteller_domain::{
    action::Action,
    piece::{CardSuit, Faction},
};

#[rstest]
fn score_points() {
    let action = Action::parse(Faction::Marquise, &format!("+3")).expect("couldn't parse action");

    let expected = Action::ScorePoints {
        faction: Faction::Marquise,
        amount: 3,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn score_points_out_of_turn() {
    let action = Action::parse(Faction::Marquise, &format!("V+3")).expect("couldn't parse action");

    let expected = Action::ScorePoints {
        faction: Faction::Vagabond(1),
        amount: 3,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn lose_points() {
    let action = Action::parse(Faction::Eyrie, &format!("-3")).expect("couldn't parse action");

    let expected = Action::LosePoints {
        faction: Faction::Eyrie,
        amount: 3,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn activate_dominance() {
    let action =
        Action::parse(Faction::Marquise, &format!("#rdom")).expect("couldn't parse action");

    let expected = Action::ActivateDominance {
        faction: Faction::Marquise,
        coalition: None,
        suit: CardSuit::Rabbit,
        commentary: None,
    };

    assert_eq!(expected, action);
}
