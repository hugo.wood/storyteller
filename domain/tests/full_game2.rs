use indoc::indoc;
use rstest::{fixture, rstest};
use storyteller_domain::{
    game::GameRecord,
    piece::Faction,
    state::{PlayerScore, State, Turn},
};

#[fixture]
fn game1() -> &'static str {
    indoc! { r#"
    Map: lake
    Deck: enp
    Players:
      M: GFletch
      O: Humberto
      C: JAMman
      D: Harriet
      E: LinenMaster

    E: (6w,b)->7/#charismatic->$/3#->E/lf->1
    D: (2w,t)->1/3w->5/2w->9/3#->D
    C: (w,t)->9/w->(1,8,9)/3#->C
    O: w->1/3w->12/m=(3,3,3)/(#rc,#mc,#bs)->O
    M: 2w->(3,8,10)/w->(1,2,4,5,6,7,9,11,12)/(tk,bw)->8/bs->3/br->10/3#->M
    //R1
    M: 3w->O$p/#bsO->M/t->3/t3->/bs->10/+1/#btunM->/t->3/t3->/br->2/+1/#bsM->/(w,lf)1->10/#->M/w9->3/#->M
    O: (3w,3Mw)$p->$f/Mw$f->$c/#ba->O/Mw$f->$c/#finform->O/Mw$f->$c/3w12->1/w$f->$c/xD1/r20/2Dw1->/w$f->$c/#bdom->O/w$f->$c/xD1/Dt1->/+1
    C: #bxC->/%x->$i/+1/t9^te/+1/#D->C/#bboatC->/w->(4,9,10,12)/(w,lf)10->12/#->C/w9->12/w12->/t->12/2#->C
    D: w->0/w->0/w5->11/(#ma,#rswap)^/#marshal->$/+1/#->D
    E: #rtE->$m/#bmE->$b/2w->7/4w7->11/xM7/r33/(w,Mw)7->/b->11/+1/#->E
    //R2
    M: t->(3,10)/2w3->12/w10->8/t3->/t10->/br->12/+2/w->(2,10,12)/2#->M/#mleagueM->
    O: 2w->$p/2w$p->$f/(3w,3Mw)$c->$f/2w$f->/(w,tr)->1/+2
    C: #msC->/%s->$i/+2/t12^te/+2/#M->C/#rcoffinC->/w->(1,3,7,11)/2w1->9/2w9->3/w3->/t->3/3#->C/(#mengrave,#mmurine)C->
    D: w->0/2w->0/(#rswap,#ma,#fa)^/#brig->$/+2/#->D
    E: #mbE->$m/2w->11/3w11->6/w6->11/xC7/r32/(w,Cw)7->/b->6/+2/2#->E
    //R3
    M: t->(3,10)/#^C{snare?}/#M->C/w8->3/(w,lf)12->10/#->M/t3->/t10->/bs->12/+2/w->(2,10,12)/2#->M/#mdom->@
    O: 2w->$p/+3/2w$p->$f/Mw$f->$c/5w1->11/Mw$f->$c/xE11/#raE->/#baO->/r11/(w,Ew)11->/Mw$f->$c/xE11/r32/(2w,3Ew)11->/2w$f->/(w,tr)->11/+2/w$f->$c/xE11/Eb11->/+1/2w$f->$cr/#mcO->/%c->$i/+3/m=(4,4,4)
    C: #fhC->/%h->$i/+2/#rmC->/%m->$i/+1/t3^tr/+3/#rcharmC->/w->(1,3,7,11)/w7->11/w11->/t->11/xO1/Otr1->/+1/3#->C/#fpropC->
    D: w->0/w->0/#bbD->/t->11/4w0->11/3w0->11/xO11/r30/3Ow11->/xO11/Otr11->/+1/(#rswap,#ma,#fa)^/#mayor->$/+2/#->D
    E: #bsaboE->$r/4w->7/3w7->6/2w6->4/xM6/r10/#mmM->/Mw6->8/b->4/+2/2#->E
    //R4
    M: t->(3,10,12)/xC12/r10/Cw12->/xC12/r33/(w,Cw,Cte)12->/+1/w->(2,10,12)/2#->M/#rpartM->
    O: 2w->$p/2w$cr->$f/(w,3Mw)$c->$f/2w$p->$f/3Mw$f->$c/(#bsabo,#mt,#ft)->O/2w$f->$cr/#rcO->/%c->$i/+3/3w$f->$c/(#ffalse,#fm,#rtun)->O/(#finform,#rtun,#bsabo)O->
    C: #ffalseC->$i/t11^ts/+3/#ffalse$i->/Mw3->8/#fcC->/w->(4,9,10,12)/(w,lf)10->12/#->C/xM3/r21/(w,Mw,Mbs)3->/+1/xM3/r11/Mt3->/+1/w4->/t->4
    D: 3w->0/xC11/r21/(w,2Cw)11->/xC11/Cts11->/+1/2w5->4/xC4/r22/(2w,Cw,Ct)4->/+1/xM11/r00/(#rswap,#fa)^/#captain->$/+1/#->D
    E: #fsE->$m/#bcplansE->$x/2w->6/2w->4/w7->2/3w6->11/2w4->5/xD11/#baD->/2w11->/r00/xD11/r10/Dw11->/b->5/+3/2#->E
    //R5
    M: t->(10,12)/4w2->8/4w8->3/2t12->/bs->3/+2/xC3/r21/(w,2Cw)3->/2#->M/(#finform,#mleague)M->/##
    O: 2w->$p/2w$p->$f/2w$cr->$f/(3w,3Mw)$c->$f/3w$f->/3w->1/w$f->$c/3w1->5/Mw$f->$c/xE5/r21/(w,2Ew)5->/2w$f->/(w,tm)->5/+2/Mw$f->$cm/#ftO->/%t->$i/+2/Mw$f->$cr/#fmO->/%m->$i/+1/w$f->$c/xE5/Eb5->/+1
    C: (#bsabo,#fpart)C->$i/#fdomC->@/w->(4,9,10,12)/4w9->12/xM12/r11/#rdomC->@/w12->/#finformM->/2Mw12->8/xM12/r32/(2Mw,Mbs,Mbr)12->{C forgot to remove 2w}/+2/2#->C
    D: w->0/#rswapD->/t->7/4w0->7/xC9/#rcharmC->/(w,Cte)9->/+1/3w11->5/xO5/r20/2Ow5->/xO5/r10/Ow5->/xO5/Otm5->/+1/xE7/r11/(w,Ew)7->/xE7/r21/(w,Ew,Eb)7->/+1/(#ma,#fa)^/#foremole->$/+1/#->D
    E: #bsoupE->$m/4w->4/w4->6/4w4->6/5w6->11/3w11->7/xD7/r32/(2w,2Dw,Dt)7->/+1/xD11/#baD->/2w11->/r00/xM2/Mbr2->/+1/b->2/+2/2#->E
    //R6
    M: t->(3,10)/#rbM->/%b->$i/+1/w10->8/4w8->3/5w3->9/3w9->1/#bemiM->/3t10->/bs->1/+2/t3->3{forgot}/br->1/+1/#->M
    O: 2w->$f/Mw$cr->$f/Mw$cm->$f/(2w,Mw)$c->$f/Mw$f->$cm/#mtO->/%t->$i/+2/(4w,2Mw)$f->$c/(#ra,#bsabo,#bs,#ffalse,#rc,#mm)->O/(#mm,#rc)O->/#bdomO->@
    C: #fhC->/w->(4,9,10,12)/xE4/r32/(w,Ew,Eb)4->/+1/w12->/t->12/(w,lf)12->10/#->C/#->C
    D: 3w->0/2w5->4/3w0->11/3w11->6/xC4/r21/(#rtun,#mc)C->/(2w,Cw)4->/xE6/r30/(2Ew,Eb)6->/+1/#->D
    E: #bbE->$r/6w->2/5w2->7/w7->10/4w7->11/(w,lf)10->11/#->E/xD11/r22/(2w,2Dw)11->/xD11/r20/(Dw,Dt)11->/+1/xM11/r22/(w,Mw)11->/b->11/+1/#->E
    //R7
    M: t->(1,3,10)/#mpartC->$i/w4->5/2t3->/t10->/bs->5/+3/t1->/bw->5/+2/#->M
    O: 2w->$f/Mw$cm->$f/(4w,2Mw)$c->$f/4w$f->/4w->11/Mw$f->$c/xE11/r30/3Ew11->/Mw$f->$c/xE11/Eb11->/+1/Mw$f->$c/4w11->5/w$f->$c/xM5/r32/#bxM->/(3w,Mbw)5->/#mmurineM->/2Mw5->8/+1/w$f->$c/xM5/(w,Mbs)5->/+1
    C: t12^te/+2/w1->5/w5->11/(w,lf)11->10/#->C/w10->/t->10
    D: 2w->0/#fbD->/t->4/2w0->4/w6->4/2w6->11/w11->7/(#ma,#fa,#ba)^/#banker->$/+2
    E: #rtunE->$m/6w->2/7w2->7/3w7->11/2w7->10/2w7->10/(3w,lf)10->11/#->E/xD7/r10/Dw7->/#^C{bomb?}/#E->C/xM10/r32/(w,Mw)10->/xD11/r32/(w,Dw)11->/b->11/+1/#->E
    //R8
    M: t->(1,3,10)/t1->/t3->/t10->/bs->9/+3/w->(1,10)/xC3/Ctr3->/+1/Cw->(9,12)/#->M
    O: 2w->$f/(2w,2Mw)$c->$f/2Mw$f->/(w,tr)->1/+2/2w$f->/2w->1/Mw$f->$c/xM1/r10/w$f->$c/xM1/r21/w1->/#rmM->/2Mw1->8/w$f->/w->10/#bsabo^C{raid?}/#bsaboO->C/#bs^C{snare?}/#bsO->C/#ffalse^C{extortion?}/Ct10->/+1
    C: #bsaboC->/w->(1,3,7,11)/(w,lf)11->10/#->C/##/w3->9/w9->/t->9/2w10->/t->10
    D: 2w->0/#fcD->/t->9/2w0->9/3w4->0/3w0->9/xM9/r33/(2w,2Mw,Mbs)9->/+1/xC9/r32/(3w,2Cw,Ct)9->/+1/w5->4/#maD->/+1/#->D
    E: #bboatE->$b/#rpart->$i/6w->11/3w11->5/5w11->5/7w5->1/2w1->9/w9->1/xM1/r33/(w,2Mw,Mbs,Mbr)1->{actually 1Mw}/+2/xO1/r00/(#mengrave,#mx)E->/Ow1->/xO1/r10/(Ow,Otr)1->/+1/b->(1,7)/+3/2#->E
    //R9
    M: t->(3,10)/xC10/r00/w10->/xC10/r31/(2w,2Cw,Ct)10->/+1/3w8->10/2w8->3/#->M
    O: 2w->$f/(w,Mw)$c->$f/(2w,Mw)$f->$c/(#fh,#ba,#rcharm)->O/w$f->$c/(w,lf)10->12/#ba->O/(#fh,#rcharm)O->
    C: #bsabo$i->/#rpartE$i->/#fpropC->/w->(4,9,10,12)/xD4/r32/(#rcoffin,#bs,#mleague)C->/(w,Dw,Dt)4->/+1/xD9/r21/(w,Dw,Dt)9->/+1/w10->7/xE7/r32/(w,Ew,Eb)7->/+1
    D: 3w->0/#->D
    E: #mcE->$m/6w->11/w11->7/3w11->5/w5->1/2w5->1/w1->9/w9->12/xO12/r30/Ow12->/xC7/r33/(w,Cw)7->/xC1/r32/(w,Cw)1->/b->(5,9)/+4/2#->E
    //R10
    M: t->(3,10)/2w3->8/2w8->2/xE2/r10/#rswapM->/(Ew,Eb)2->/+1/2t3->/bs->2/+2/#->M
    O: 2w->$f/(3w,Mw)$c->$f/(5w,Mw)$f->$c/(#mm,#rc,#rtun,#mleague,#bx)->O/(#mm,#rc,#rtun,#ffalse,#bx)O->
    C: #ffalse$i->/3Mw10->8/(7w,lf)12->10/#->C/xM10/r30/(2Mw,Mt)10->/+1/xM10/r30/(Mbs,Mbr,Mt)10->/+3
    "#}
}

#[fixture]
fn turns(game1: &str) -> Vec<Turn> {
    let game_record: GameRecord = game1.parse().expect("couldn't parse game record");
    game_record.states()
}

#[fixture]
fn final_state(turns: Vec<Turn>) -> State {
    turns
        .into_iter()
        .last()
        .unwrap()
        .states
        .into_iter()
        .last()
        .unwrap()
}

#[rstest]
#[case(Faction::Corvid, 30)]
#[case(Faction::Riverfolk, 29)]
#[case(Faction::Marquise, 26)]
#[case(Faction::Eyrie, 25)]
#[case(Faction::Duchy, 19)]
fn scores(final_state: State, #[case] faction: Faction, #[case] points: usize) {
    assert_eq!(PlayerScore::Points(points), final_state.score(&faction));
}

#[rstest]
fn action_count(turns: Vec<Turn>) {
    assert_eq!(
        723,
        turns.iter().map(|turn| turn.states.len()).sum::<usize>()
    );
}

#[rstest]
fn turn_count(turns: Vec<Turn>) {
    assert_eq!(53, turns.len());
}

#[rstest]
fn parse_print(game1: &str) {
    let expected: GameRecord = game1.parse().expect("couldn't parse game record");
    let text = expected.to_string();
    println!("{}", text);
    let actual: GameRecord = text.parse().expect("couldn't parse actual");

    assert_eq!(expected, actual);
}
