use indoc::indoc;
use rstest::{fixture, rstest};
use storyteller_domain::{
    game::GameRecord,
    piece::{Building, Faction, Location, Piece, Suit, Token},
    state::{PlayerScore, State, Turn},
};

#[fixture]
fn game1() -> &'static str {
    indoc! { r#"
    // Root Winter Tournament - Round 2, Game 3
    // Played: 25 November 2020.
    // https://www.youtube.com/watch?v=cg3NbiR7szc

    // Commentary

    // Watch the Eyrie's Decree. With no one forcing him to Turmoil, it became a force to be reckoned with. Updated for Rootlog V2.4.

    Map: Lake
    Deck: E&P
    Clearings: mfrrfmrfrmmf
    DraftPool: MDEAL
    Players:
      A: Waterman121
      E: Russano the Wise
      M: Nevakanezah
      L: LilyG
    Winner: E

    M: tk->4/(br,bw)->6/bs->4/w->(1,2,4,5,6,7,8,9,10,11,12)/3#->M
    E: (b,6w)->3/#despot->$/3#->E
    A: 3#->$/3#->A
    L: (bf,4w)->2/w->(7,8,10)/m=f/3#->L

    L: #f^/w->8/#f^/bf->8/#m^/w->10/2#->L
    M: t->4/t4->/bs->11/+1/#mM->/t->11/t11->/br->5/+1/#->M
    E: #fE->$m/6w3->12/3w12->3/b->12/+1/#->E
    A: (#b,#r,#f)$->/t->(7,2)/+1/2#A->$/#->A

    L: m=hf/#f^/w->9/#fL->/+2/2#m^/2bm->10/#r^/w->3/#ffalseL->$i/3#->L/#L->
    M: t->(4,11)/%t->$i/+2/xA7/#rM->A$/At7->/+1/t4->/t11->/bs->5/+2/w->(5,6)/#bM->/w1->11/#->M/2w11->7/#->M
    E: #rE->$r/#fpartE->$i/w->3/2w12->9/w3->12/b->9/+2/2#->E
    A: 2#b$->/(Lbf,4Lw,Mw)2->/#L->/+1/(bf,w)->2/w->$o/#A->$/w->2/2#->A

    L: m=r/#m^/w->10/#mL->/+2/2#f^/2w->12/2#->L
    M: t->(4,11)/%t->$i/+2/t4->/t11->/br->11/+2/w->(5,11,6)/xE9/(w,Ew)9->/2#->M {Wood wasn't placed at 5 in Birdsong.}
    E: #fE->$r/w->(9,12)/w12->9/2w9->1/b->1/+3/2#->E
    A: 2#fA->/2w->$o/#A->$/2w2->7/2w->2/2#->A {Skipped Birdsong intentionally.}

    L: m=f/#b^/w->$a/2#f^/2w->12/#mL->/+2/#m^/w->11/%x->$i/+1/2#->L
    M: t->(4,5,11)/#saboM->$i/2#bM->/w->(5,6,11)/3w5->1/w10->7/xE1/(2Ew,Eb)1->/+1/xA7/(2w,Aw)7->/w6->7/w5->11/2#->M {Wood in 5 was placed after Evening. Bird spends were off-screen, but assumed to happen.}
    E: #mE->$x/#rE->$r/w->12/2w->9/w12->9/3w9->1/xM1/#mM->/3Mw1->4/w1->/b->1/+3/2#->E
    A: 2#r$->/t->7/+1/2#A->$/w2->7/2w->2/2#->A

    L: m=hf/#b^/w->$a/#f^/bf->12/#fL->/+2/2#r^/2w->7/3#->L/#L-> {Uncertain how many cards were discarded at the end, if any.}
    M: t->(5,4,11)/%b->$i/+1/3w7->11/3w11->1/#->M/xE1/Ew1->/t5->/2t11->/bs->1/+3/#bM->/xE1/(Ew,Eb)1->/+1/2#->M {Bird spend is assumed to have happened off-screen.}
    E: #mE->$x/#mE->$m/#cplansE->$i/2w->9/w->12/w12->9/4w9->1/4w1->11/#->E/xM11/#maM->/#mdomM->L$/2Mw11->4/2w11->/xM11/(w,Mw,Mbs)11->/+2/b->11/+3/2#->E
    A: (#r,#b)$->/3Lw7->/(br,w)->7/w->$o/#m$->/t->11/+1/w7->6/w2->8/w6->/t->6/w8->/t->8/+3/##/3#->A

    L: #mdom$->@/m=r/2w$a->/w3->9/xE9/Eb9->/+1/#b^/w->$a/#r^/w->9/(#f,#m)L->/+4/#f^/w->5/3#->L/#L->
    M: t->(5,1,4)/#bM->/w->(5,11,6)/xA11/At11->/^A/#->A$/+1/t5->/2t4->/bs->1/+3/4w4->5/4w5->11/2#->M
    E: #fE->$x/#rE->$r/#bboatE->$i/w->12/3w->3/4w3->12/5w12->11/3w11->5/xM5/(w,Mbs)5->/+2/xM11/Mw11->/xM11/#mM->/3Mw11->4/b->5/+3/2#->E
    A: 4#A->$/w7->6/w2->10/w->(2,7)/3#->A {Intentionally skipped Birdsong again.}

    L: m=m/#bdomL->/#mdom@->L/(#m,#f)L->/+4/#r^/br->9/#f^/w->5/#b^/w->$a/%b->$i/+1/3#->L
    M: t->4/2t->1/#sabo$i->/#cplansE$i->/4w6->11/3w4->5/xE5/xE11/2Ew11->/2#->M
    E: #fE->$r/#bE->$x/%m->$i/+1/#leagueE->$i/2w->5/3w->3/4w5->1/%me/xM1/#mM->/3Mw1->4/w1->/w1->5/w3->8/#->A$/xM1/(Mt,2Mbs)1->/+4/xM1/2Mt1->/+3/xA8/#baA->/w8->/xL11/b->1/+3 {"Look at this decreeee!"}

    "#}
}

#[fixture]
fn turns(game1: &str) -> Vec<Turn> {
    let game_record: GameRecord = game1.parse().expect("couldn't parse game record");
    game_record.states()
}

#[fixture]
fn final_state(turns: Vec<Turn>) -> State {
    turns
        .into_iter()
        .last()
        .unwrap()
        .states
        .into_iter()
        .last()
        .unwrap()
}

#[rstest]
#[case(Faction::Eyrie, 30)]
#[case(Faction::Marquise, 21)]
#[case(Faction::Cult, 19)]
#[case(Faction::Alliance, 7)]
fn scores(final_state: State, #[case] faction: Faction, #[case] points: usize) {
    assert_eq!(PlayerScore::Points(points), final_state.score(&faction));
}

#[rstest]
#[case(1, Piece::Warrior(Faction::Eyrie), 2)]
#[case(1, Piece::Building(Building::Roost), 1)]
#[case(2, Piece::Warrior(Faction::Alliance), 2)]
#[case(2, Piece::Building(Building::Base { suit: Suit::Fox }), 1)]
#[case(2, Piece::Token(Token::Sympathy), 1)]
#[case(3, Piece::Warrior(Faction::Eyrie), 4)]
#[case(3, Piece::Building(Building::Roost), 1)]
#[case(4, Piece::Warrior(Faction::Marquise), 5)]
#[case(4, Piece::Token(Token::Keep), 1)]
#[case(4, Piece::Token(Token::Wood), 2)]
#[case(4, Piece::Building(Building::Sawmill), 1)]
#[case(5, Piece::Warrior(Faction::Eyrie), 1)]
#[case(5, Piece::Building(Building::Roost), 1)]
#[case(5, Piece::Warrior(Faction::Marquise), 4)] // actually 3
#[case(5, Piece::Building(Building::Recruiter), 1)]
#[case(5, Piece::Token(Token::Wood), 1)]
#[case(5, Piece::Warrior(Faction::Cult), 2)]
#[case(6, Piece::Warrior(Faction::Alliance), 1)]
#[case(6, Piece::Token(Token::Sympathy), 1)]
#[case(6, Piece::Building(Building::Workshop), 1)]
#[case(6, Piece::Building(Building::Recruiter), 1)]
#[case(7, Piece::Warrior(Faction::Alliance), 2)]
#[case(7, Piece::Building(Building::Base { suit: Suit::Rabbit }), 1)]
#[case(7, Piece::Token(Token::Sympathy), 1)]
#[case(8, Piece::Warrior(Faction::Cult), 2)] // actually 3
#[case(8, Piece::Building(Building::Garden { suit: Suit::Fox }), 1)]
#[case(8, Piece::Warrior(Faction::Marquise), 1)]
#[case(9, Piece::Warrior(Faction::Cult), 3)] // actually 2
#[case(9, Piece::Building(Building::Garden { suit: Suit::Rabbit }), 1)]
#[case(10, Piece::Warrior(Faction::Cult), 3)]
#[case(10, Piece::Building(Building::Garden { suit: Suit::Mouse }), 2)]
#[case(10, Piece::Warrior(Faction::Alliance), 1)]
#[case(11, Piece::Warrior(Faction::Eyrie), 1)]
#[case(11, Piece::Building(Building::Roost), 1)]
#[case(11, Piece::Warrior(Faction::Marquise), 5)]
#[case(11, Piece::Building(Building::Recruiter), 1)]
#[case(11, Piece::Warrior(Faction::Cult), 1)]
#[case(12, Piece::Warrior(Faction::Eyrie), 2)]
#[case(12, Piece::Building(Building::Roost), 1)]
#[case(12, Piece::Warrior(Faction::Marquise), 1)]
#[case(12, Piece::Warrior(Faction::Cult), 4)]
#[case(12, Piece::Building(Building::Garden { suit: Suit::Fox }), 1)]
fn clearing_pieces(
    final_state: State,
    #[case] clearing_number: usize,
    #[case] piece: Piece,
    #[case] n: usize,
) {
    assert_eq!(
        n,
        final_state.piece_count(&Location::Clearing(clearing_number), &piece)
    );
}

#[rstest]
fn action_count(turns: Vec<Turn>) {
    assert_eq!(
        338,
        turns.iter().map(|turn| turn.states.len()).sum::<usize>()
    );
}

#[rstest]
fn turn_count(turns: Vec<Turn>) {
    assert_eq!(31, turns.len());
}

#[rstest]
fn parse_print(game1: &str) {
    let expected: GameRecord = game1.parse().expect("couldn't parse game record");
    let text = expected.to_string();
    println!("{}", text);
    let actual: GameRecord = text.parse().expect("couldn't parse actual");

    assert_eq!(expected, actual);
}
