use rstest::rstest;
use storyteller_domain::{
    action::{Action, Pieces},
    piece::{Building, Card, CardName, CardSuit, Faction, Item, Location, Piece, Roll, Token},
};

#[rstest]
fn place_keep() {
    let action = Action::parse(Faction::Marquise, "tk->1").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Token(Token::Keep), 1)],
        at: vec![Location::Clearing(1)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn place_keep_and_warriors() {
    let action = Action::parse(Faction::Marquise, "(tk,2w)->1").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![
            (Piece::Token(Token::Keep), 1),
            (Piece::Warrior(Faction::Marquise), 2),
        ],
        at: vec![Location::Clearing(1)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn place_wood() {
    let action = Action::parse(Faction::Marquise, "t->1").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Token(Token::Wood), 1)],
        at: vec![Location::Clearing(1)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("b", Item::Bag)]
#[case("m", Item::Boot)]
#[case("c", Item::Coins)]
#[case("x", Item::Crossbow)]
#[case("h", Item::Hammer)]
#[case("s", Item::Sword)]
#[case("t", Item::Tea)]
fn craft_item(#[case] i: &str, #[case] expected_item: Item) {
    let action =
        Action::parse(Faction::Marquise, &format!("%{i}->$i")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Item(expected_item, None), 1)],
        at: vec![Location::Crafted(Faction::Marquise)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case(
    "#bboat",
    Card::DeckCard(Some(CardSuit::Bird), Some(CardName::BoatBuilders))
)]
fn craft_improvement(#[case] c: &str, #[case] expected_card: Card) {
    let action =
        Action::parse(Faction::Marquise, &format!("{c}M->$i")).expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Card(expected_card), 1)]),
        from: Location::Hand(Faction::Marquise),
        to: Location::Crafted(Faction::Marquise),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
#[case("r", Building::Recruiter)]
#[case("s", Building::Sawmill)]
#[case("w", Building::Workshop)]
fn build(#[case] c: &str, #[case] expected_building: Building) {
    let action =
        Action::parse(Faction::Marquise, &format!("b{c}->1")).expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Building(expected_building), 1)],
        at: vec![Location::Clearing(1)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn remove_two_wood() {
    let action = Action::parse(Faction::Marquise, "2t10->").expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![(Piece::Token(Token::Wood), 2)]),
        from: Location::Clearing(10),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn move_single_warrior() {
    let action: Action = Action::parse(Faction::Marquise, "w1->5").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Marquise), 1)]),
        from: Location::Clearing(1),
        to: Location::Clearing(5),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn move_many_warriors() {
    let action: Action = Action::parse(Faction::Marquise, "5w1->5").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Marquise), 5)]),
        from: Location::Clearing(1),
        to: Location::Clearing(5),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn initiate_battle() {
    let action: Action = Action::parse(Faction::Marquise, "xE7").expect("couldn't parse action");

    let expected = Action::Battle {
        attacker: Faction::Marquise,
        defender: Faction::Eyrie,
        in_: 7,
        loot: false,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn roll_battle_dice(#[values(0, 1, 2, 3)] d1: Roll, #[values(0, 1, 2, 3)] d2: Roll) {
    let action: Action =
        Action::parse(Faction::Marquise, &format!("r{d1}{d2}")).expect("couldn't parse action");

    let expected = Action::RollBattleDice {
        attacker: d1,
        defender: d2,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn play_ambush() {
    let action = Action::parse(Faction::Marquise, "#faM->").expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![(
            Piece::Card(Card::DeckCard(Some(CardSuit::Fox), Some(CardName::Ambush))),
            1,
        )]),
        from: Location::Hand(Faction::Marquise),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn remove_warriors() {
    let action =
        Action::parse(Faction::Marquise, &format!("2w5->")).expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Marquise), 2)]),
        from: Location::Clearing(5),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn remove_enemy_warriors() {
    let action =
        Action::parse(Faction::Marquise, &format!("5Ew5->")).expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![(Piece::Warrior(Faction::Eyrie), 5)]),
        from: Location::Clearing(5),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn remove_many_pieces() {
    let action = Action::parse(Faction::Marquise, &format!("(2w,bs,5Ew,Eb)5->"))
        .expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![
            (Piece::Warrior(Faction::Marquise), 2),
            (Piece::Building(Building::Sawmill), 1),
            (Piece::Warrior(Faction::Eyrie), 5),
            (Piece::Building(Building::Roost), 1),
        ]),
        from: Location::Clearing(5),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn hawks_for_hire() {
    let action = Action::parse(Faction::Marquise, "#bboatM->").expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![(
            Piece::Card(Card::DeckCard(
                Some(CardSuit::Bird),
                Some(CardName::BoatBuilders),
            )),
            1,
        )]),
        from: Location::Hand(Faction::Marquise),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn recruit() {
    let action = Action::parse(Faction::Marquise, "w->12").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Warrior(Faction::Marquise), 1)],
        at: vec![Location::Clearing(12)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn draw_card() {
    let action = Action::parse(Faction::Marquise, "#->M").expect("couldn't parse action");

    let expected = Action::Place {
        pieces: vec![(Piece::Card(Card::DeckCard(None, None)), 1)],
        at: vec![Location::Hand(Faction::Marquise)],
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn discard_card() {
    let action = Action::parse(Faction::Marquise, "#M->").expect("couldn't parse action");

    let expected = Action::Remove {
        pieces: Pieces::Specific(vec![(Piece::Card(Card::DeckCard(None, None)), 1)]),
        from: Location::Hand(Faction::Marquise),
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn discard_dominance_card() {
    let action = Action::parse(Faction::Marquise, "#rdomM->@").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(
            Piece::Card(Card::DeckCard(
                Some(CardSuit::Rabbit),
                Some(CardName::Dominance),
            )),
            1,
        )]),
        from: Location::Hand(Faction::Marquise),
        to: Location::AvailableDominanceCards,
        commentary: None,
    };

    assert_eq!(expected, action);
}

#[rstest]
fn claim_dominance_card() {
    let action = Action::parse(Faction::Marquise, "#rdom@->M").expect("couldn't parse action");

    let expected = Action::Move {
        pieces: Pieces::Specific(vec![(
            Piece::Card(Card::DeckCard(
                Some(CardSuit::Rabbit),
                Some(CardName::Dominance),
            )),
            1,
        )]),
        from: Location::AvailableDominanceCards,
        to: Location::Hand(Faction::Marquise),
        commentary: None,
    };

    assert_eq!(expected, action);
}
