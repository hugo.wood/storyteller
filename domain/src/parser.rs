use crate::{
    action::{Action, Pieces, Quantity},
    game::{DraftPoolEntry, GameRecord, PlayerRecord, TurnRecord},
    piece::{
        Building, Card, CardName, CardSuit, DuchyMinister, EyrieCard, EyrieDecreeColumn,
        EyrieLeader, Faction, HundredsMood, HundredsPiece, Item, ItemState, KeepersCard,
        KeepersRetinueColumn, Landmark, Location, Marker, Piece, Plot, Quest, Relationship, Relic,
        Roll, Suit, Token, VagabondCharacter,
    },
};
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{digit1, newline, none_of, not_line_ending, one_of, space0},
    combinator::{eof, fail, map, opt, recognize, success, value},
    multi::{many1, separated_list1},
    sequence::{delimited, preceded, terminated, tuple},
    Finish, IResult,
};
use std::str::FromStr;

fn comment(input: &str) -> IResult<&str, ()> {
    value((), tuple((tag("//"), not_line_ending)))(input)
}

fn comment_newline(input: &str) -> IResult<&str, ()> {
    value((), many1(tuple((space0, opt(comment), newline))))(input)
}

fn commentary(input: &str) -> IResult<&str, String> {
    map(
        preceded(space0, delimited(tag("{"), many1(none_of("}")), tag("}"))),
        |cs| cs.into_iter().collect(),
    )(input)
}

fn suit(input: &str) -> IResult<&str, Suit> {
    alt((
        value(Suit::Fox, tag("f")),
        value(Suit::Mouse, tag("m")),
        value(Suit::Rabbit, tag("r")),
    ))(input)
}

fn faction(input: &str) -> IResult<&str, Faction> {
    alt((
        value(Faction::Marquise, tag("M")),
        value(Faction::Eyrie, tag("E")),
        value(Faction::Alliance, tag("A")),
        map(preceded(tag("V"), opt(number)), |n| {
            Faction::Vagabond(n.unwrap_or(1))
        }),
        value(Faction::Cult, tag("L")),
        value(Faction::Riverfolk, tag("O")),
        value(Faction::Duchy, tag("D")),
        value(Faction::Corvid, tag("C")),
        value(Faction::Hundreds, tag("H")),
        value(Faction::Keepers, tag("K")),
    ))(input)
}

fn faction_full_name(input: &str) -> IResult<&str, Faction> {
    alt((
        value(Faction::Marquise, tag("marquise")),
        value(Faction::Eyrie, tag("eyrie")),
        value(Faction::Alliance, tag("alliance")),
        map(preceded(tag("vagabond"), opt(number)), |n| {
            Faction::Vagabond(n.unwrap_or(1))
        }),
        value(Faction::Cult, tag("cult")),
        value(Faction::Riverfolk, tag("riverfolk")),
        value(Faction::Duchy, tag("duchy")),
        value(Faction::Corvid, tag("corvid")),
        value(Faction::Hundreds, tag("hundreds")),
        value(Faction::Keepers, tag("keepers")),
    ))(input)
}

fn building<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Building> {
    preceded(tag("b"), move |input| match f {
        Faction::Marquise => alt((
            value(Building::Recruiter, tag("r")),
            value(Building::Sawmill, tag("s")),
            value(Building::Workshop, tag("w")),
        ))(input),
        Faction::Eyrie => success(Building::Roost)(input),
        Faction::Alliance => map(suit, |suit| Building::Base { suit })(input),
        Faction::Cult => map(suit, |suit| Building::Garden { suit })(input),
        Faction::Duchy => alt((
            value(Building::Citadel, tag("c")),
            value(Building::Market, tag("m")),
        ))(input),
        Faction::Hundreds => success(Building::Stronghold)(input),
        Faction::Keepers => map(tuple((relic, relic)), |(obverse, reverse)| {
            Building::Waystation { obverse, reverse }
        })(input),
        _ => fail(input),
    })
}

fn plot(input: &str) -> IResult<&str, Plot> {
    alt((
        value(Plot::Bomb, tag("b")),
        value(Plot::Extortion, tag("e")),
        value(Plot::Raid, tag("r")),
        value(Plot::Snare, tag("s")),
    ))(input)
}

fn relic(input: &str) -> IResult<&str, Relic> {
    alt((
        value(Relic::Figure, tag("f")),
        value(Relic::Jewelry, tag("j")),
        value(Relic::Tablet, tag("t")),
    ))(input)
}

fn token<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&str, Token> {
    preceded(tag("t"), move |input| match f {
        Faction::Marquise => alt((value(Token::Keep, tag("k")), success(Token::Wood)))(input),
        Faction::Alliance => success(Token::Sympathy)(input),
        Faction::Riverfolk => map(suit, |suit| Token::TradePost { suit })(input),
        Faction::Duchy => success(Token::Tunnel)(input),
        Faction::Corvid => map(opt(plot), |p| Token::Plot { plot: p })(input),
        Faction::Hundreds => success(Token::Mob)(input),
        Faction::Keepers => map(tuple((opt(number), relic)), |(n, r)| Token::Relic {
            relic: r,
            value: n,
        })(input),
        _ => fail(input),
    })
}

fn item(input: &str) -> IResult<&str, Item> {
    alt((
        value(Item::Bag, tag("b")),
        value(Item::Boot, tag("m")),
        value(Item::Coins, tag("c")),
        value(Item::Crossbow, tag("x")),
        value(Item::Hammer, tag("h")),
        value(Item::Sword, tag("s")),
        value(Item::Tea, tag("t")),
        value(Item::Torch, tag("f")),
    ))(input)
}

fn item_state(input: &str) -> IResult<&str, ItemState> {
    alt((
        value(ItemState::Exhausted, tag("e")),
        value(ItemState::Refreshed, tag("r")),
    ))(input)
}

fn card_suit(input: &str) -> IResult<&str, CardSuit> {
    alt((
        value(CardSuit::Bird, tag("b")),
        value(CardSuit::Fox, tag("f")),
        value(CardSuit::Mouse, tag("m")),
        value(CardSuit::Rabbit, tag("r")),
    ))(input)
}

fn card_name(input: &str) -> IResult<&str, CardName> {
    alt((
        alt((
            value(CardName::Armorers, tag("armor")),
            value(CardName::BetterBankBurrow, tag("bank")),
            value(CardName::BrutalTactics, tag("brutal")),
            value(CardName::CommandWarren, tag("command")),
            value(CardName::Cobbler, tag("cob")),
            value(CardName::Codebreakers, tag("codeb")),
            value(CardName::Favor, tag("favor")),
            value(CardName::RoyalClaim, tag("royal")),
            value(CardName::Sappers, tag("sap")),
            value(CardName::ScoutingParty, tag("scout")),
            value(CardName::StandAndDeliver, tag("stand")),
            value(CardName::TaxCollector, tag("tax")),
        )),
        alt((
            value(CardName::BoatBuilders, tag("boat")),
            value(CardName::CharmOffensive, tag("charm")),
            value(CardName::CoffinMakers, tag("coffin")),
            value(CardName::CorvidPlanners, tag("cplans")),
            value(CardName::EyrieEmigree, tag("emi")),
            value(CardName::FalseOrders, tag("false")),
            value(CardName::Partisans, tag("part")),
            value(CardName::Informants, tag("inform")),
            value(CardName::LeagueOfAdventurousMice, tag("league")),
            value(CardName::MasterEngravers, tag("engrave")),
            value(CardName::MurineBroker, tag("murine")),
            value(CardName::PropagandaBureau, tag("prop")),
            value(CardName::Saboteurs, tag("sabo")),
            value(CardName::SoupKitchens, tag("soup")),
            value(CardName::SwapMeet, tag("swap")),
            value(CardName::Tunnels, tag("tun")),
        )),
        alt((
            value(CardName::Ambush, tag("a")),
            value(CardName::Dominance, tag("dom")),
            value(CardName::Bag, tag("b")),
            value(CardName::Boot, tag("m")),
            value(CardName::Coins, tag("c")),
            value(CardName::Crossbow, tag("x")),
            value(CardName::Hammer, tag("h")),
            value(CardName::Sword, tag("s")),
            value(CardName::Tea, tag("t")),
        )),
    ))(input)
}

fn deck_card(input: &str) -> IResult<&str, Card> {
    map(tuple((opt(card_suit), opt(card_name))), |(s, n)| {
        Card::DeckCard(s, n)
    })(input)
}

fn vagabond_character(input: &str) -> IResult<&str, VagabondCharacter> {
    alt((
        value(VagabondCharacter::Adventurer, tag("adventurer")),
        value(VagabondCharacter::Arbiter, tag("arbiter")),
        value(VagabondCharacter::Harrier, tag("harrier")),
        value(VagabondCharacter::Ranger, tag("ranger")),
        value(VagabondCharacter::Ronin, tag("ronin")),
        value(VagabondCharacter::Scoundrel, tag("scoundrel")),
        value(VagabondCharacter::Thief, tag("thief")),
        value(VagabondCharacter::Tinker, tag("tinker")),
        value(VagabondCharacter::Vagrant, tag("vagrant")),
    ))(input)
}

fn duchy_minister(input: &str) -> IResult<&str, DuchyMinister> {
    alt((
        value(DuchyMinister::Foremole, tag("foremole")),
        value(DuchyMinister::Captain, tag("captain")),
        value(DuchyMinister::Marshal, tag("marshal")),
        value(DuchyMinister::Brigadier, tag("brig")),
        value(DuchyMinister::Banker, tag("banker")),
        value(DuchyMinister::Mayor, tag("mayor")),
        value(DuchyMinister::DuchessOfMud, tag("mud")),
        value(DuchyMinister::BaronOfDirt, tag("dirt")),
        value(DuchyMinister::EarlOfStone, tag("stone")),
    ))(input)
}

fn hundreds_mood(input: &str) -> IResult<&str, HundredsMood> {
    alt((
        value(HundredsMood::Bitter, tag("bitter")),
        value(HundredsMood::Grandiose, tag("grandiose")),
        value(HundredsMood::Jubilant, tag("jubilant")),
        value(HundredsMood::Lavish, tag("lavish")),
        value(HundredsMood::Relentless, tag("relent")),
        value(HundredsMood::Rowdy, tag("rowdy")),
        value(HundredsMood::Stubborn, tag("stubborn")),
        value(HundredsMood::Wrathful, tag("wrath")),
    ))(input)
}

fn faction_card(input: &str) -> IResult<&str, Card> {
    alt((
        map(
            alt((
                value(EyrieLeader::Builder, tag("builder")),
                value(EyrieLeader::Charismatic, tag("charismatic")),
                value(EyrieLeader::Commander, tag("commander")),
                value(EyrieLeader::Despot, tag("despot")),
            )),
            Card::EyrieLeader,
        ),
        value(Card::EyrieCard(EyrieCard::LoyalVizier), tag("viz")),
        map(vagabond_character, Card::VagabondCharacter),
        map(duchy_minister, Card::DuchyMinister),
        map(hundreds_mood, Card::HundredsMood),
        value(
            Card::KeepersCard(KeepersCard::FaithfulRetainer),
            tag("faith"),
        ),
    ))(input)
}

fn quest(input: &str) -> IResult<&str, Quest> {
    alt((
        value(Quest::Errand, tag("errand")),
        value(Quest::Escort, tag("escort")),
        value(Quest::ExpelBandits, tag("bandits")),
        value(Quest::FendOffABear, tag("bear")),
        value(Quest::Fundraising, tag("funds")),
        value(Quest::GiveASpeech, tag("speech")),
        value(Quest::GuardDuty, tag("guard")),
        value(Quest::LogisticsHelp, tag("logs")),
        value(Quest::RepairAShed, tag("shed")),
    ))(input)
}

fn quest_card(input: &str) -> IResult<&str, Card> {
    map(tuple((suit, quest)), |(s, q)| Card::Quest(s, q))(input)
}

fn card(input: &str) -> IResult<&str, Card> {
    preceded(tag("#"), alt((quest_card, faction_card, deck_card)))(input)
}

fn warrior<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Faction> {
    value(f, tag("w"))
}

fn pawn<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Faction> {
    value(f, tag("p"))
}

fn warlord<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, ()> {
    move |input| match f {
        Faction::Hundreds => value((), tag("ww"))(input),
        _ => fail(input),
    }
}

fn faction_piece<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Piece> {
    move |input| {
        let (input, f) = alt((faction, success(f)))(input)?;
        alt((
            value(Piece::HundredsPiece(HundredsPiece::Warlord), warlord(f)),
            map(warrior(f), Piece::Warrior),
            map(pawn(f), Piece::Pawn),
            map(building(f), Piece::Building),
            map(token(f), Piece::Token),
        ))(input)
    }
}

fn landmark(input: &str) -> IResult<&str, Landmark> {
    alt((
        value(Landmark::Ferry, tag("f")),
        value(Landmark::LostCity, tag("c")),
        value(Landmark::Tower, tag("t")),
    ))(input)
}

fn marker(input: &str) -> IResult<&str, Marker> {
    alt((
        value(Marker::Ruins, tag("r")),
        value(Marker::ClosedPath, tag("c")),
    ))(input)
}

fn piece<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Piece> {
    alt((
        faction_piece(f),
        map(
            preceded(tag("%"), tuple((opt(item_state), item))),
            |(s, i)| Piece::Item(i, s),
        ),
        map(card, Piece::Card),
        map(preceded(tag("l"), landmark), Piece::Landmark),
        map(preceded(tag("m"), marker), Piece::Marker),
    ))
}

fn quantity<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Quantity> {
    map(tuple((opt(number), piece(f))), |(n, p)| (p, n.unwrap_or(1)))
}

fn quantities<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Vec<Quantity>> {
    alt((
        map(quantity(f), |q| vec![q]),
        delimited(tag("("), separated_list1(tag(","), quantity(f)), tag(")")),
    ))
}

fn pieces<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Pieces> {
    alt((
        map(tag("_"), |_| Pieces::All),
        map(quantities(f), Pieces::Specific),
    ))
}

fn number(input: &str) -> IResult<&str, usize> {
    map(digit1, |ds: &str| ds.parse().unwrap())(input)
}

fn faction_board_location<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Location> {
    move |input| {
        let (input, f) = alt((faction, success(f)))(input)?;
        let (input, _) = tag("$")(input)?;
        match f {
            Faction::Marquise => value(Location::Crafted(f), tag("i"))(input),
            Faction::Eyrie => alt((
                value(Location::Crafted(f), tag("i")),
                map(
                    alt((
                        value(EyrieDecreeColumn::Recruit, tag("r")),
                        value(EyrieDecreeColumn::Move, tag("m")),
                        value(EyrieDecreeColumn::Battle, tag("x")),
                        value(EyrieDecreeColumn::Build, tag("b")),
                    )),
                    Location::EyrieDecree,
                ),
                success(Location::EyrieLeader),
            ))(input),
            Faction::Alliance => alt((
                value(Location::Crafted(f), tag("i")),
                value(Location::AllianceOfficers, tag("o")),
                success(Location::AllianceSupporters),
            ))(input),
            Faction::Vagabond(i) => alt((
                value(Location::VagabondSatchel(i), tag("s")),
                value(Location::VagabondTrack(i), tag("t")),
                value(Location::VagabondDamaged(i), tag("d")),
                value(Location::VagabondCompletedQuests(i), tag("q")),
                success(Location::VagabondCharacter(i)),
            ))(input),
            Faction::Cult => alt((
                value(Location::Crafted(f), tag("i")),
                value(Location::CultAcolytes, tag("a")),
                success(Location::CultLostSouls),
            ))(input),
            Faction::Riverfolk => alt((
                value(Location::Crafted(f), tag("i")),
                value(Location::RiverfolkPayments, tag("p")),
                value(Location::RiverfolkFunds, tag("f")),
                map(preceded(tag("c"), opt(suit)), Location::RiverfolkCommitted),
            ))(input),
            Faction::Duchy => alt((
                value(Location::Crafted(f), tag("i")),
                success(Location::DuchySwayedMinisters),
            ))(input),
            Faction::Corvid => value(Location::Crafted(f), tag("i"))(input),
            Faction::Hundreds => alt((
                value(Location::Crafted(f), tag("i")),
                value(Location::HundredsHoard, tag("h")),
                success(Location::HundredsMood),
            ))(input),
            Faction::Keepers => alt((
                value(Location::Crafted(f), tag("i")),
                map(
                    alt((
                        value(KeepersRetinueColumn::Move, tag("m")),
                        value(KeepersRetinueColumn::BattleDelve, tag("x")),
                        value(KeepersRetinueColumn::MoveRecover, tag("r")),
                    )),
                    Location::KeepersRetinue,
                ),
            ))(input),
        }
    }
}

fn map_location(input: &str) -> IResult<&str, Location> {
    map(separated_list1(tag("_"), number), |clearings| {
        if clearings.len() == 1 {
            Location::Clearing(clearings[0])
        } else if clearings.len() == 2 {
            Location::Path(clearings[0], clearings[1])
        } else {
            Location::Forest(clearings)
        }
    })(input)
}

fn location<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Location> {
    alt((
        value(Location::Burrow, tag("0")),
        map_location,
        value(Location::AvailableDominanceCards, tag("@")),
        value(Location::AvailableQuests, tag("Q")),
        faction_board_location(f),
        map(faction, Location::Hand),
    ))
}

fn locations<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Vec<Location>> {
    alt((
        map(location(f), |l| vec![l]),
        delimited(tag("("), separated_list1(tag(","), location(f)), tag(")")),
    ))
}

fn place<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&str, Action> {
    map(
        tuple((quantities(f), tag("->"), locations(f), opt(commentary))),
        |(qs, _, to, c)| Action::Place {
            pieces: qs,
            at: to,
            commentary: c,
        },
    )
}

fn re_move<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((
            pieces(f),
            location(f),
            tag("->"),
            opt(location(f)),
            opt(commentary),
        )),
        |(qs, from, _, to, c)| match to {
            None => Action::Remove {
                pieces: qs,
                from,
                commentary: c,
            },
            Some(to) => Action::Move {
                pieces: qs,
                from,
                to,
                commentary: c,
            },
        },
    )
}

fn remove_from_game<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((quantities(f), opt(location(f)), tag("->*"), opt(commentary))),
        |(qs, from, _, c)| Action::RemoveFromGame {
            pieces: qs,
            from,
            commentary: c,
        },
    )
}

fn reveal<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((opt(quantities(f)), tag("^"), opt(faction), opt(commentary))),
        move |(qs, _, other, c)| Action::Reveal {
            faction: f,
            pieces: qs,
            to: other,
            commentary: c,
        },
    )
}

fn flip<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((piece(f), location(f), tag("^"), piece(f), opt(commentary))),
        move |(obverse, l, _, reverse, c)| Action::Flip {
            faction: f,
            at: l,
            obverse,
            reverse,
            commentary: c,
        },
    )
}

fn swap<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((
            piece(f),
            location(f),
            tag("<->"),
            piece(f),
            location(f),
            opt(commentary),
        )),
        |(p1, l1, _, p2, l2, c)| Action::Swap {
            pieces: [p1, p2],
            locations: [l1, l2],
            commentary: c,
        },
    )
}

fn battle<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((tag("x"), opt(tag("l")), faction, number, opt(commentary))),
        move |(_, loot, defender, c, co)| Action::Battle {
            attacker: f,
            defender,
            in_: c,
            loot: loot.is_some(),
            commentary: co,
        },
    )
}

fn roll(input: &str) -> IResult<&str, Roll> {
    map(recognize(one_of("0123")), |d: &str| d.parse().unwrap())(input)
}

fn roll_battle_dice(input: &str) -> IResult<&str, Action> {
    map(
        tuple((tag("r"), roll, roll, opt(commentary))),
        |(_, d1, d2, c)| Action::RollBattleDice {
            attacker: d1,
            defender: d2,
            commentary: c,
        },
    )(input)
}

fn roll_mob_die(input: &str) -> IResult<&str, Action> {
    map(tuple((tag("r"), suit, opt(commentary))), |(_, s, c)| {
        Action::RollMobDie {
            suit: s,
            commentary: c,
        }
    })(input)
}

fn item_state_change<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    alt((
        map(
            tuple((pieces(f), tag("e"), opt(location(f)), opt(commentary))),
            move |(qs, _, l, c)| Action::Exhaust {
                faction: f,
                pieces: qs,
                in_: l,
                commentary: c,
            },
        ),
        map(
            tuple((pieces(f), tag("r"), opt(location(f)), opt(commentary))),
            move |(qs, _, l, c)| Action::Refresh {
                faction: f,
                pieces: qs,
                in_: l,
                commentary: c,
            },
        ),
    ))
}

fn move_item_state_change<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    alt((
        map(
            tuple((
                pieces(f),
                location(f),
                tag("-e>"),
                location(f),
                opt(commentary),
            )),
            move |(qs, from, _, to, c)| Action::MoveExhaust {
                pieces: qs,
                from,
                to,
                commentary: c,
            },
        ),
        map(
            tuple((
                pieces(f),
                location(f),
                tag("-r>"),
                location(f),
                opt(commentary),
            )),
            move |(qs, from, _, to, c)| Action::MoveRefresh {
                pieces: qs,
                from,
                to,
                commentary: c,
            },
        ),
    ))
}

fn points<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((
            alt((faction, success(f))),
            one_of("+-"),
            number,
            opt(commentary),
        )),
        |(f, o, n, c)| match o {
            '+' => Action::ScorePoints {
                faction: f,
                amount: n,
                commentary: c,
            },
            '-' => Action::LosePoints {
                faction: f,
                amount: n,
                commentary: c,
            },
            _ => unreachable!(),
        },
    )
}

fn relationship(input: &str) -> IResult<&str, Relationship> {
    alt((
        value(Relationship::Hostile, tag("h")),
        value(Relationship::Indifferent, tag("0")),
        value(Relationship::Allied1, tag("1")),
        value(Relationship::Allied2, tag("2")),
        value(Relationship::Allied, tag("a")),
    ))(input)
}

fn set_relationship<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((
            alt((faction, success(f))),
            tag("m"),
            faction,
            tag("="),
            relationship,
            opt(commentary),
        )),
        move |(f, _, other, _, r, c)| Action::SetRelationship {
            faction: f,
            with: other,
            to: r,
            commentary: c,
        },
    )
}

fn set_outcast<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((tag("m"), tag("="), opt(tag("h")), suit, opt(commentary))),
        move |(_, _, h, s, c)| Action::SetOutcast {
            faction: f,
            suit: s,
            hated: h.is_some(),
            commentary: c,
        },
    )
}

fn set_service_prices<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((
            tag("m"),
            tag("="),
            tag("("),
            number,
            tag(","),
            number,
            tag(","),
            number,
            tag(")"),
            opt(commentary),
        )),
        move |(_, _, _, hand_card, _, riverboats, _, mercenaries, _, c)| Action::SetServicePrices {
            faction: f,
            hand_card,
            riverboats,
            mercenaries,
            commentary: c,
        },
    )
}

fn activate_dominance<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    map(
        tuple((
            tag("#"),
            card_suit,
            tag("dom"),
            opt(faction),
            opt(commentary),
        )),
        move |(_, s, _, other, c)| Action::ActivateDominance {
            faction: f,
            coalition: other,
            suit: s,
            commentary: c,
        },
    )
}

fn refill_draw_pile(input: &str) -> IResult<&str, Action> {
    map(tuple((tag("##"), opt(commentary))), |(_, c)| {
        Action::RefillDrawPile { commentary: c }
    })(input)
}

fn action<'a>(f: Faction) -> impl FnMut(&'a str) -> IResult<&'a str, Action> {
    alt((
        item_state_change(f),
        move_item_state_change(f),
        place(f),
        remove_from_game(f),
        re_move(f),
        reveal(f),
        flip(f),
        swap(f),
        battle(f),
        roll_battle_dice,
        roll_mob_die,
        points(f),
        set_relationship(f),
        set_outcast(f),
        set_service_prices(f),
        activate_dominance(f),
        refill_draw_pile,
    ))
}

fn entry<'a, P, R>(t: &'static str, p: P) -> impl FnMut(&'a str) -> IResult<&'a str, R>
where
    P: FnMut(&'a str) -> IResult<&'a str, R>,
{
    delimited(tuple((tag(t), tag(":"), space0)), p, comment_newline)
}

fn player(input: &str) -> IResult<&str, PlayerRecord> {
    map(
        tuple((
            comment_newline,
            tag("  "),
            faction,
            tag(":"),
            space0,
            opt(not_line_ending),
        )),
        |(_, _, f, _, _, n)| PlayerRecord {
            faction: Some(f),
            name: n.map(ToOwned::to_owned),
        },
    )(input)
}

fn players(input: &str) -> IResult<&str, Vec<PlayerRecord>> {
    map(many1(player), |ps| ps.into_iter().collect())(input)
}

fn turn(input: &str) -> IResult<&str, TurnRecord> {
    let (input, f) = faction(input)?;
    let (input, _) = tuple((tag(":"), space0))(input)?;
    map(separated_list1(tag("/"), action(f)), move |actions| {
        TurnRecord {
            faction: f,
            actions,
        }
    })(input)
}

fn draft_pool_entry(input: &str) -> IResult<&str, DraftPoolEntry> {
    alt((
        map(faction, DraftPoolEntry::Faction),
        map(
            preceded(tag("#"), vagabond_character),
            DraftPoolEntry::VagabondCharacter,
        ),
    ))(input)
}

fn game_record(input: &str) -> IResult<&str, GameRecord> {
    map(
        delimited(
            opt(comment_newline),
            tuple((
                entry("Map", not_line_ending),
                entry("Deck", not_line_ending),
                opt(entry("Clearings", many1(suit))),
                opt(entry("Landmarks", many1(landmark))),
                opt(entry("DraftPool", many1(draft_pool_entry))),
                opt(entry("Players", players)),
                opt(entry("Winner", faction)),
                separated_list1(comment_newline, turn),
            )),
            opt(comment_newline),
        ),
        |(map_name, deck_name, clearing_suits, landmarks, draft_pool, players, winner, turns)| {
            GameRecord {
                map_name: map_name.to_owned(),
                deck_name: deck_name.to_owned(),
                clearing_suits,
                landmarks: landmarks.unwrap_or_default(),
                draft_pool,
                players: players.unwrap_or_default(),
                turns,
                winner,
            }
        },
    )(input)
}

#[derive(Debug)]
pub struct ParseError(String);

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

impl FromStr for Faction {
    type Err = ParseError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match terminated(faction_full_name, eof)(input.as_ref()).finish() {
            Ok((_, result)) => Ok(result),
            Err(e) => Err(ParseError(e.to_string())),
        }
    }
}

impl Action {
    pub fn parse(faction: Faction, input: &str) -> Result<Self, ParseError> {
        match terminated(action(faction), eof)(input.as_ref()).finish() {
            Ok((_, result)) => Ok(result),
            Err(e) => Err(ParseError(e.to_string())),
        }
    }
}

impl FromStr for GameRecord {
    type Err = ParseError;

    fn from_str(input: &str) -> Result<Self, ParseError> {
        match terminated(game_record, eof)(input.as_ref()).finish() {
            Ok((_, result)) => Ok(result),
            Err(e) => Err(ParseError(e.to_string())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_draft_pool() {
        let input = "DraftPool: MDEAL\n";
        terminated(entry("DraftPool", many1(faction)), eof)(input)
            .finish()
            .expect("could'nt parse draft pool");
    }
}
