use crate::piece::{CardSuit, ClearingNumber, Faction, Location, Piece, Relationship, Roll, Suit};
use serde::{Deserialize, Serialize};

pub type Quantity = (Piece, usize);

#[derive(PartialEq, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Pieces {
    All,
    Specific(Vec<Quantity>),
}

#[derive(PartialEq, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Action {
    Place {
        pieces: Vec<Quantity>,
        at: Vec<Location>,
        commentary: Option<String>,
    },
    Move {
        pieces: Pieces,
        from: Location,
        to: Location,
        commentary: Option<String>,
    },
    Remove {
        pieces: Pieces,
        from: Location,
        commentary: Option<String>,
    },
    RemoveFromGame {
        pieces: Vec<Quantity>,
        from: Option<Location>,
        commentary: Option<String>,
    },
    Reveal {
        faction: Faction,
        pieces: Option<Vec<Quantity>>,
        to: Option<Faction>,
        commentary: Option<String>,
    },
    Flip {
        faction: Faction,
        at: Location,
        obverse: Piece,
        reverse: Piece,
        commentary: Option<String>,
    },
    Swap {
        pieces: [Piece; 2],
        locations: [Location; 2],
        commentary: Option<String>,
    },
    Battle {
        attacker: Faction,
        defender: Faction,
        in_: ClearingNumber,
        loot: bool,
        commentary: Option<String>,
    },
    RollBattleDice {
        attacker: Roll,
        defender: Roll,
        commentary: Option<String>,
    },
    RollMobDie {
        suit: Suit,
        commentary: Option<String>,
    },
    Exhaust {
        faction: Faction,
        pieces: Pieces,
        in_: Option<Location>,
        commentary: Option<String>,
    },
    MoveExhaust {
        pieces: Pieces,
        from: Location,
        to: Location,
        commentary: Option<String>,
    },
    Refresh {
        faction: Faction,
        pieces: Pieces,
        in_: Option<Location>,
        commentary: Option<String>,
    },
    MoveRefresh {
        pieces: Pieces,
        from: Location,
        to: Location,
        commentary: Option<String>,
    },
    ScorePoints {
        faction: Faction,
        amount: usize,
        commentary: Option<String>,
    },
    LosePoints {
        faction: Faction,
        amount: usize,
        commentary: Option<String>,
    },
    SetRelationship {
        faction: Faction,
        with: Faction,
        to: Relationship,
        commentary: Option<String>,
    },
    SetOutcast {
        faction: Faction,
        suit: Suit,
        hated: bool,
        commentary: Option<String>,
    },
    SetServicePrices {
        faction: Faction,
        hand_card: usize,
        riverboats: usize,
        mercenaries: usize,
        commentary: Option<String>,
    },
    ActivateDominance {
        faction: Faction,
        coalition: Option<Faction>,
        suit: CardSuit,
        commentary: Option<String>,
    },
    RefillDrawPile {
        commentary: Option<String>,
    },
}
