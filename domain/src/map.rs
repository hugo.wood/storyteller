use crate::piece::ClearingNumber;

pub struct Path {
    pub from: ClearingNumber,
    pub to: ClearingNumber,
    closed: bool,
}

impl Path {
    pub fn new(from: ClearingNumber, to: ClearingNumber) -> Self {
        Path {
            from,
            to,
            closed: false,
        }
    }

    pub fn closed(from: ClearingNumber, to: ClearingNumber) -> Self {
        Path {
            from,
            to,
            closed: true,
        }
    }

    pub fn is_closed(&self) -> bool {
        self.closed
    }
}

pub struct Map {
    pub ruins: Vec<ClearingNumber>,
    pub paths: Vec<Path>,
}

impl Map {
    pub fn autumn() -> Self {
        Self {
            ruins: vec![6, 10, 11, 12],
            paths: vec![
                (1, 5),
                (1, 9),
                (1, 10),
                (2, 5),
                (2, 6),
                (2, 10),
                (3, 6),
                (3, 7),
                (3, 11),
                (4, 8),
                (4, 9),
                (4, 12),
                (6, 11),
                (7, 8),
                (7, 12),
                (9, 12),
                (10, 12),
                (11, 12),
            ]
            .into_iter()
            .map(|(from, to)| Path::new(from, to))
            .collect(),
        }
    }

    pub fn winter() -> Self {
        Self {
            ruins: vec![8, 9, 11, 12],
            paths: vec![
                (1, 5),
                (1, 10),
                (2, 6),
                (2, 7),
                (2, 12),
                (3, 7),
                (3, 8),
                (3, 12),
                (4, 9),
                (4, 10),
                (4, 11),
                (5, 6),
                (8, 9),
                (8, 12),
                (9, 11),
            ]
            .into_iter()
            .map(|(from, to)| Path::new(from, to))
            .collect(),
        }
    }

    pub fn lake() -> Self {
        Self {
            ruins: vec![5, 10, 11, 12],
            paths: vec![
                (1, 5),
                (1, 9),
                (2, 7),
                (2, 8),
                (2, 10),
                (3, 8),
                (3, 9),
                (3, 12),
                (4, 5),
                (4, 6),
                (5, 11),
                (6, 11),
                (6, 7),
                (7, 10),
                (8, 10),
                (9, 12),
            ]
            .into_iter()
            .map(|(from, to)| Path::new(from, to))
            .collect(),
        }
    }

    pub fn mountain() -> Self {
        Self {
            ruins: vec![9, 10, 11, 12],
            paths: vec![
                Path::new(1, 8),
                Path::new(1, 9),
                Path::closed(2, 5),
                Path::new(2, 6),
                Path::new(2, 11),
                Path::new(3, 6),
                Path::closed(3, 7),
                Path::new(3, 11),
                Path::new(4, 8),
                Path::new(4, 12),
                Path::closed(5, 9),
                Path::new(5, 10),
                Path::new(5, 11),
                Path::closed(6, 11),
                Path::new(7, 12),
                Path::closed(8, 9),
                Path::new(9, 10),
                Path::new(9, 12),
                Path::new(10, 11),
                Path::new(10, 12),
                Path::closed(11, 12),
            ],
        }
    }
}
