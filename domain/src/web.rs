use crate::{
    action::Action, game::GameRecord, parser::ParseError, piece::Faction, printer::FactionScoped,
};
use gloo_utils::format::JsValueSerdeExt;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

impl From<ParseError> for JsError {
    fn from(e: ParseError) -> Self {
        Self::new(&e.to_string())
    }
}

#[wasm_bindgen(js_name = parseAction)]
pub fn parse_action(faction: JsValue, input: &str) -> Result<JsValue, JsError> {
    let action = Action::parse(faction.into_serde()?, input.into())?;
    Ok(JsValue::from_serde(&action)?)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TurnText {
    faction: Faction,
    actions: Vec<String>,
}

#[wasm_bindgen]
pub fn turns_text(game_record: JsValue) -> Result<JsValue, JsError> {
    let game_record: GameRecord = game_record.into_serde()?;
    let result: Vec<TurnText> = game_record
        .turns
        .iter()
        .map(|turn| TurnText {
            faction: turn.faction,
            actions: turn
                .actions
                .iter()
                .map(|action| FactionScoped::with(turn.faction, action).to_string())
                .collect(),
        })
        .collect();
    Ok(JsValue::from_serde(&result)?)
}

#[wasm_bindgen]
pub fn states(game_record: JsValue) -> Result<JsValue, JsError> {
    let game_record: GameRecord = game_record.into_serde()?;
    let states = game_record.states();
    Ok(JsValue::from_serde(&states)?)
}

#[wasm_bindgen]
pub fn load_game(input: String) -> Result<JsValue, JsError> {
    let game_record: GameRecord = input.parse()?;
    Ok(JsValue::from_serde(&game_record)?)
}

#[wasm_bindgen]
pub fn dump_game(game_record: JsValue) -> Result<String, JsError> {
    let game_record: GameRecord = game_record.into_serde()?;
    Ok(game_record.to_string())
}
